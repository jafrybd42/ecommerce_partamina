<?php
// Heading
$_['heading_title']          = '<p style="color:green">Advance Support Email </p>';
$_['heading_title1']          = 'Advance Support Email';

// Text
$_['text_extension']           = 'Extension';
$_['text_success']              = 'Success: You have modified products!';
$_['text_subject']              = 'Recent Comment on Your Request';
$_['text_list']                 = 'Ticket List';
$_['text_add']                  = 'Email Template';
$_['text_yes']                  = 'Yes';
$_['text_no']                   = 'No';
$_['text_submit']               = 'Submit';
$_['text_user']                 = 'User Information';
$_['tab_customer']     			 = 'Email For Customer';
$_['tab_admin']       			 = ' Email For Admin';

$_['text_upload']            	 = 'Attachments';
$_['text_history']               = 'History';
$_['text_comment']               = 'Comment';
$_['text_create']                = 'Customer email when new ticket submitted';
$_['text_create_admin']          = 'Admin email when new ticket submitted';
$_['text_comment_admin']         = 'Customer email when ticket status changed';
$_['text_comment']               = 'Customer email when a new comment is added to ticket';
// Column
$_['column_subject']          = 'Subject';
$_['column_department']      = 'Department';
$_['column_message']         = 'Message';
$_['column_customer']        = 'Customer name';
$_['column_date_added']      = 'Date Added';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_notfiy']          = 'Notify';
$_['column_action']          = 'Action';
$_['column_date_added']          = 'Date Added';
$_['column_status']          = 'Status';

$_['column_notify']          = 'Notify';
$_['column_comment']          = 'Comment';

// Entry

$_['entry_subject']           = 'Subject';
$_['entry_message']           = 'Message ';
$_['entry_sort']              = 'Sort order';
$_['button_add']              = 'Add Status';


// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
