<?php
// Heading
$_['heading_title']          = '<p style="color:green">Advance Support Department<?p>';
$_['heading_title1']          = 'Advance Support Department';

// Text
$_['text_extension']   		 = 'Extensions';
$_['text_success']           = 'Success: You have modify department!';
$_['text_list']              = 'Department ';
$_['text_add']               = 'Add Product';
$_['button_save']            = 'Submit';

// Column
$_['column_subject']         = 'Subject';
$_['column_department']      = 'Department';
$_['column_message']         = 'message';
$_['column_customer']         = 'customer name';
$_['column_date_added']      = 'Date Added';
$_['column_status']         = 'Status';
$_['column_action']         = 'Action';
$_['button_save']          = 'Save';

// Entry
$_['entry_name']             = 'Department Name';


// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_sku']               = 'Stock Keeping Unit';


// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';

