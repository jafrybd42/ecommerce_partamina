<?php
class ControllerExtensionModuleSupportEmail extends Controller {
	private $error = array();

	public function index() {
	$this->load->language('extension/module/support_email');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('setting/setting');
		$this->load->model('extension/report/support');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			$this->model_setting_setting->editSetting('support', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');
	

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_status_list'] = $this->language->get('text_status_list');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_color'] = $this->language->get('entry_color');
		$data['entry_sort'] = $this->language->get('entry_sort');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' =>  $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/category', 'user_token=' . $this->session->data['user_token'], true);
	
	

		$data['cancel'] =  $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['category_status'])) {
			$data['category_status'] = $this->request->post['category_status'];
		} else {
			$data['category_status'] = $this->config->get('category_status');
		}
       $this->load->language('extension/module/support_email');

		
        $this->load->model('extension/report/support');

		
		$this->installt();
		$this->getform();
		
		
		
	}
	
	public function installt() { 
	$this->model_extension_report_support->install();
   }
	public function add(){
		$this->load->model('extension/report/support');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			
			$this->model_extension_report_support->addEmail($this->request->post);
	
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		
		}
		
	$this->getForm();
		 
	}
		
	public function edit(){
		$this->load->model('extension/report/support');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			
			$this->model_extension_report_support->editStatus($this->request->post,$this->request->get['status_id']);
	
			$this->response->redirect($this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url, true));
		
		}
		
	$this->getForm();
		 
	}
	
   
	public function delete() {
		$this->load->language('extension/module/support_status');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('extension/report/support');

		if (isset($this->request->post['selected']) ) {
			foreach ($this->request->post['selected'] as $status_id) {
				$this->model_extension_report_support->deletestatus($status_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/support_status', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		
	}
	
		protected function getForm() {
		
		
		$this->load->language('extension/module/support_email');
		$this->load->model('extension/report/support');
		
		$data['heading_title'] = $this->language->get('heading_title');

			
		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_create'] = $this->language->get('text_create');
		$data['text_create_admin'] = $this->language->get('text_create_admin');
		$data['text_comment_admin'] = $this->language->get('text_comment_admin');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_comment_subject'] = $this->language->get('text_comment_subject');
		
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['entry_message'] = $this->language->get('entry_message');
		
		$data['tab_customer'] = $this->language->get('tab_customer');
		$data['tab_admin'] = $this->language->get('tab_admin');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
	
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		
		

		$url = '';

		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' =>  $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/support_email', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);
		
		
		if (!isset($this->request->get['status_id'])) {
			$data['action'] = $this->url->link('extension/module/support_email/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		 } else {
			$data['action'] = $this->url->link('extension/module/support_email/edit', 'user_token=' . $this->session->data['user_token'] . '&status_id=' . $this->request->get['status_id'] . $url, 'SSL');
		}

		
		$data['cancel'] =  $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);


		if (($this->request->server['REQUEST_METHOD'] != 'POST')) {
			 $email_info = $this->model_extension_report_support->getemail();
		}

		$data['user_token'] = $this->session->data['user_token'];

		
	$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();
		if (isset($this->request->post['admin_description'])) {
			$data['admin_description'] = $this->request->post['admin_description'];
		} elseif (!empty($status_info)) {
			$data['admin_description'] = $status_info[0]['name'];
		} else {
			$data['admin_description'] = '';
		}

		
	//create ticket 
		if (isset($this->request->post['create_ticket'])) {
			$data['create_ticket'] = $this->request->post['create_ticket'];
		} elseif (!empty($email_info)) {
			
			 $email_info = $this->model_extension_report_support->getemail();
			 foreach($email_info as $email){
			
				if($email['flag']==2){
				
				 $data['create_ticket'][$email['language_id']]=array(
				 'email_id'=>$email['email_id'],
				 'language_id'=>$email['language_id'],
				 'subject'=>$email['subject'],
				 'message'=>$email['message']
				  );
				} 
				if($email['flag']==1){
				 
				 $data['admin_create_ticket'][$email['language_id']]=array(
				 'email_id'=>$email['email_id'],
				 'language_id'=>$email['language_id'],
				 'adminsubject'=>$email['subject'],
				 'adminmessage'=>$email['message']
				 
				  );
				} 
			 }
		} else {
			$data['create_ticket'] = '';
		   }

		if (isset($this->request->post['notify'])) {
			$data['notify'] = $this->request->post['notify'];
		} elseif (!empty($email_info)) {
		   $email_info = $this->model_extension_report_support->getemail();
		   foreach($email_info as $email){
			
			//Customer Comment email
			if($email['notify']==2){
				
				$data['notify_ticket'][$email['language_id']]=array(
				 'email_id'=>$email['email_id'],
				 'language_id'=>$email['language_id'],
				 'subject'=>$email['subject'],
				 'message'=>$email['message']
				 );
			  } 
			//customer notify email
			 if($email['notify']==1){
				 
				 $data['admin_notify_ticket'][$email['language_id']]=array(
				 'email_id'=>$email['email_id'],
				 'language_id'=>$email['language_id'],
				 'adminsubject'=>$email['subject'],
				 'adminmessage'=>$email['message']
				 
				 );
			 } 
				 
		 }
			
		} 
		else {
			$data['notify'] = '';
		}

		

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/support_email', $data));
	}
	
	
	
	
	
	
	
			private function validate() {
				if($this->request->post['notify']!=' '){
		if ((utf8_strlen(trim($this->request->post['comment1'])) < 1)) {
			$this->error['comment1'] = $this->language->get('error_comment1');
		}
				}
		return !$this->error;
	}

	
	

}