<?php
class ControllerExtensionModuleDemo extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/demo');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('setting/setting');
		$this->load->model('extension/report/group');
		$this->load->model('extension/report/support');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			$this->model_setting_setting->editSetting('account', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect=$this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/account', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['account_status'])) {
			$data['account_status'] = $this->request->post['account_status'];
		} else {
			$data['account_status'] = $this->config->get('account_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/faq_group_list', $data));
		$this->installt();
		$this->getList();
		
	}
	public function installt() { 
	$this->model_extension_report_support->install();
   }
	
		public function add() {
		$this->load->language('extension/module/demo');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('extension/report/group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			
			$this->model_extension_report_group->addFAQGroup($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('extension/module/demo');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('extension/report/group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		
			$this->model_extension_report_group->editGroup($this->request->get['group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('extension/module/demo');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('extension/report/group');

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $group_id) {
				$this->model_extension_report_group->deleteGroup($group_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}
	public function addfaq() {
	
	
		$this->load->language('extension/module/demo');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('extension/report/group');

		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');
		$data['heading_title50'] = $this->language->get('heading_title50');

		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
	

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		
	
		$data['entry_attribute'] = $this->language->get('entry_attribute');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_option'] = $this->language->get('entry_option');
	

		

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_attribute_add'] = $this->language->get('button_attribute_add');
	
		
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['tab_attribute'] = $this->language->get('tab_attribute');
		

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['model'])) {
			$data['error_model'] = $this->error['model'];
		} else {
			$data['error_model'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title50'),
			'href' => $this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_add'),
			'href' => $this->url->link('extension/module/demo/addfaq', 'user_token=' . $this->session->data['user_token'] . '&group_id=' . $this->request->get['group_id'] . $url, true)
		);
		

;
		if (!isset($this->request->get['group_id'])) {
			
			$data['action'] = $this->url->link('extension/module/demo/addfaq', 'user_token=' . $this->session->data['user_token'] . $url, true);
			
		} else {
				
			$data['action'] = $this->url->link('extension/module/demo/addfaq', 'user_token=' . $this->session->data['user_token'] . '&group_id=' . $this->request->get['group_id'] . $url, true);
		}
		$data['cancel'] = $this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$this->load->model('localisation/language');
		
		

		if (isset($this->request->get['group_id'])&& ($this->request->server['REQUEST_METHOD'] == 'POST')) {
		
			$info = $this->model_extension_report_group->addFAQ($this->request->get['group_id'],$this->request->post);
			
		
			$this->response->redirect($this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}
		
		
		
		if (isset($this->request->post['product_attribute'])) {
				
			$product_attributes = $this->request->post['product_attribute'];
			
		} elseif (isset($this->request->get['group_id'])) {
			$product_attributes = $this->model_extension_report_group->getfaq($this->request->get['group_id']);
			
			
			
		} else {
			$product_attributes = array();
		}
	
		$data['languages'] = $this->model_localisation_language->getLanguages();
	
		$data['product_attributes'] = array();

	
			
		 foreach ($product_attributes[0]['product_description_data'] as $product_attribute) {
			
			
					$data['product_attributes'][] = array(
					'group_id'                  => $product_attributes[0]['group_id'],
					
					'product_attribute_description' => $product_attribute
					
			);
			
			
		}
	
	 
		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
	$this->response->setOutput($this->load->view('extension/module/questions', $data));
 }
 protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'agd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title50'),
			'href' => $this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'], true)
		);
		
		
	
			if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
					
			$this->model_setting_setting->editSetting('demo', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		
			}
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($info)) {
			$data['status'] = $info['status'];
		} else {
			$data['status'] = '';
		}
		
		$data['add'] = $this->url->link('extension/module/demo/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('extension/module/demo/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['cancel'] = $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		$data['attribute_groups'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$group_total = $this->model_extension_report_group->getTotalGroups();

		$results = $this->model_extension_report_group->getGroups($filter_data);

		foreach ($results as $result) {
			$data['faq_groups'][] = array(
				'group_id' => $result['group_id'],
				'name'               => $result['name'],
				'sort_order'         => $result['sort_order'],
				'addfaq'               => $this->url->link('extension/module/demo/addfaq', 'user_token=' . $this->session->data['user_token'] . '&group_id=' . $result['group_id'] . $url, true),
				'edit'               => $this->url->link('extension/module/demo/edit', 'user_token=' . $this->session->data['user_token'] . '&group_id=' . $result['group_id'] . $url, true)
			);
			
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');
		$data['heading_title50'] = $this->language->get('heading_title50');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_group'] = $this->language->get('column_group');
		$data['column_faq'] = $this->language->get('column_faq');
		
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_addFAQ'] = $this->language->get('button_addFAQ');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/group', 'user_token=' . $this->session->data['user_token'] . '&sort=agd.name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/group', 'user_token=' . $this->session->data['user_token'] . '&sort=ag.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/attribute_group', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($group_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($group_total - $this->config->get('config_limit_admin'))) ? $group_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $group_total, ceil($group_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/faq_group_list', $data));
	}
	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title1'] = $this->language->get('heading_title1');

		$data['text_form'] = !isset($this->request->get['attribute_group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title50'),
			'href' => $this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'], true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_add'),
			'href' => $this->url->link('extension/module/demo/edit', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (!isset($this->request->get['group_id'])) {
			$data['action'] = $this->url->link('extension/module/demo/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			
			$data['action'] = $this->url->link('extension/module/demo/edit', 'user_token=' . $this->session->data['user_token'] . '&group_id=' . $this->request->get['group_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('extension/module/demo', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['group_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$group_info = $this->model_extension_report_group->getGroup($this->request->get['group_id']);
		
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['attribute_group_description'])) {
			$data['attribute_group_description'] = $this->request->post['attribute_group_description'];
		} elseif (isset($this->request->get['attribute_group_id'])) {
		
		} else {
			$data['attribute_group_description'] = array();
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($group_info)) {
			$data['sort_order'] = $group_info[0]['sort_order'];
		} else {
			$data['sort_order'] = '';
		}
			if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($group_info)) {
			$data['name'] = $group_info[0]['name'];
		} else {
			$data['name'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/faq_group_form', $data));
	}
	
}