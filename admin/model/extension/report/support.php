<?php
class ModelExtensionReportSupport extends Model {
			
			
	public function install(){
				
	
	$this->db->query("	CREATE TABLE IF NOT EXISTS `oc_department` (
		`department_id` INT(11) NOT NULL AUTO_INCREMENT,
		department VARCHAR(100) ,
	
		PRIMARY KEY (`department_id`)
		) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");



	$this->db->query("CREATE TABLE IF NOT EXISTS  oc_support_email (
        email_id int(11) NOT NULL AUTO_INCREMENT,
       
        subject text ,
        message text ,
		language_id int(11),
		flag int(11),
		notify int(11),
        PRIMARY KEY (email_id )
		) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");
	  
	  
		$this->db->query("CREATE TABLE IF NOT EXISTS oc_support_status (   status_id int(11) NOT NULL AUTO_INCREMENT, 
        name varchar(200) , 
		color varchar(200),
        PRIMARY KEY (status_id)
      ) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;
	  ");


		$sql=$this->db->query("SELECT * FROM oc_support_status");
		  if($sql->num_rows==0){
		 
			$this->db->query("INSERT INTO `oc_support_status` (`status_id`, `name`) VALUES (1, 'open'), (2, 'pending'), (3, 'close')");
		  }

	$this->db->query("CREATE TABLE IF NOT EXISTS  oc_support_comment (
        comment_id int(11) NOT NULL AUTO_INCREMENT,
       
        support_id int(11) ,
     
        name varchar(200) ,
        comment	 text ,
        date_added 	datetime,
		status int(11),
        PRIMARY KEY (comment_id)
      ) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");

	$this->db->query("CREATE TABLE IF NOT EXISTS  oc_support_file  (
        file_id int(11) NOT NULL AUTO_INCREMENT,
       
        support_id int(11) ,
        
        file_name text ,
        date_added date ,
        
        PRIMARY KEY (file_id)
      ) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");
	
		 $this->db->query("   CREATE TABLE IF NOT EXISTS oc_support_status_histroy (
        notify_id int(11) NOT NULL AUTO_INCREMENT,
       
        support_id int(11) ,
        status_id int(11) ,
        notify int(11)  DEFAULT '0' ,
		 	message TEXT ,
        	date_added DATETIME ,
        PRIMARY KEY (notify_id)
      ) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");
	  
	  
	  $this->db->query(" CREATE TABLE IF NOT EXISTS `oc_support` (
		`support_id` INT(11) NOT NULL AUTO_INCREMENT,
		customer_id  int(11) ,
        subject  VARCHAR(100) ,
        department  int(11) ,
        message text ,
			status_id  int(11) ,
        	date_added date ,
        
        PRIMARY KEY (support_id)
		) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");




	$this->db->query("CREATE TABLE IF NOT EXISTS oc_faq_group (
        group_id int(11) NOT NULL AUTO_INCREMENT,
       
        name VARCHAR(30) ,
        sort_order int(11) ,
        PRIMARY KEY (`group_id`)
      ) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");
 
		
	$this->db->query("   CREATE TABLE IF NOT EXISTS oc_faq_support (
        faq_id int(11) NOT NULL AUTO_INCREMENT,
       
        group_id int(11) ,
        que_id int(11) ,
        language_id int(11) ,
		 question TEXT ,
        answer TEXT ,
        PRIMARY KEY (faq_id)
		) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci");


	 $this->db->query("  CREATE TABLE IF NOT EXISTS oc_questions (
        que_id int(11) NOT NULL AUTO_INCREMENT,
       
      
        group_id int(11) ,
        PRIMARY KEY (que_id)
      ) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;
	");
	  


	
	}	
	public function addDepartment($data){
				
	
		$sql= $this->db->query("INSERT INTO " . DB_PREFIX . "department SET `department` = '" .  $this->db->escape($data['title']) . "'");
		
		$department_id = $this->db->getLastId();
		
		return $department_id;
		
	}	
	
	
	public function addEmail($data){
		
	$sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "support_email ");

	
   if(!empty($sql->num_rows)){
			
		if(isset($data['create_ticket'])){
		 
			foreach ($data['create_ticket'] as $language_id => $value) {
			
				if($value['create']['customer']){
				
				$this->db->query("update  " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['create']['customer']['customer_subject']) . "', message = '" . $this->db->escape($value['create']['customer']['customer_message']) . "' WHERE flag='2'");
				
			   }
				if($value['create']['admin'])
				{
				
				$this->db->query("update  " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['create']['admin']['admin_subject']) . "', message = '" . $this->db->escape($value['create']['admin']['admin_message']) . "'WHERE flag='1'");
				}
			}
		}	
		if(isset($data['notify'])){
			
			foreach ($data['notify'] as $language_id => $value) {
	
//Comment email		
		if($value['notify']['customer']){
				
				$this->db->query("update  " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['notify']['customer']['customer_subject']) . "', message = '" . $this->db->escape($value['notify']['customer']['customer_message']) . "' WHERE notify='2'");
				
			   }
			   
	//notify email		   
				if($value['notify']['admin'])
				{
				
				$this->db->query("update  " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['notify']['admin']['admin_subject']) . "', message = '" . $this->db->escape($value['notify']['admin']['admin_message']) . "'WHERE notify='1'");
				}
			}
		}

	  }
	else{
				
		if(isset($data['create_ticket'])){
				
			foreach ($data['create_ticket'] as $language_id => $value) {
			
				if($value['create']['customer']){
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['create']['customer']['customer_subject']) . "', message = '" . $this->db->escape($value['create']['customer']['customer_message']) . "',flag='2'");
				}
				if($value['create']['admin']){
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['create']['admin']['admin_subject']) . "', message = '" . $this->db->escape($value['create']['admin']['admin_message']) . "',flag='1'");
				}
			}
		}
		
		
		if(isset($data['notify'])){
				
			foreach ($data['notify'] as $language_id => $value) {
			
			if($value['notify']['customer']){
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['notify']['customer']['customer_subject']) . "', message = '" . $this->db->escape($value['notify']['customer']['customer_message']) . "',notify=2");
				}
		   if($value['notify']['admin']){
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "support_email SET language_id = '" . (int)$language_id . "', subject = '" . $this->db->escape($value['notify']['admin']['admin_subject']) . "', message = '" . $this->db->escape($value['notify']['admin']['admin_message']) . "',notify=1");
			 }
			}
		}
			
		
		
		
	}
	  
		$email_id = $this->db->getLastId();
		
		return $email_id;
		
	}

	 public function addStatus($data){
				
		$sql= $this->db->query("INSERT INTO " . DB_PREFIX . "support_status SET `name` = '" .  $this->db->escape($data['status']) . "',`color` = '" .  $this->db->escape($data['color']) . "'");
		
		$status_id = $this->db->getLastId();
		
		return $status_id;
		
	} 
	public function editStatus($data,$status_id){
	
		
		$sql= $this->db->query("UPDATE  " . DB_PREFIX . "support_status  SET`name` = '" .  $this->db->escape($data['status']) . "',`color` = '" .  $this->db->escape($data['color']) . "'where status_id='".(int)$status_id."'");
		
		$status_id = $this->db->getLastId();
		
		return $status_id;
		
	} 
	
	public function addNotify($data,$support_id){
				
			$sql= $this->db->query("INSERT INTO " . DB_PREFIX . "support_status_histroy SET `message` = '" .  $this->db->escape($data['comment1']) . "',`status_id` = '" .  (int)$data['status'] . "',support_id = '".(int)$support_id."',notify='".(int)$data['notify']."',date_added=NOW()");
				
				
				
				$status_detail=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_status WHERE status_id = '".(int)$data['status']."'");
			$status=$status_detail->rows;
				
				
				
			//notify customer email	detail
			$email_Detail_admin=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_email WHERE notify = '1'");
			$customer_email_info=$email_Detail_admin->rows;
			
				
				
				if (isset($data['status'])) {
					$this->db->query("UPDATE " . DB_PREFIX . "support SET `status_id` = '" .  (int)$data['status'] . "' WHERE support_id = '" . (int)$support_id . "'");
				}
				
				if($data['notify']=='1'){
			
				$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "support s LEFT JOIN " . DB_PREFIX . "department d ON (s.department = d.department_id) LEFT JOIN " . DB_PREFIX . "customer c ON (s.customer_id = c.customer_id) WHERE s.support_id = '" . (int)$support_id. "'");
			
				$custmEmail=$customer->rows;
			
			
				
				$message  = html_entity_decode($customer_email_info[0]['message'],ENT_QUOTES, 'UTF-8') . "\n\n<br>";
				$message .= $this->language->get('entry_comment') . ': ' . $data['comment1'] . "\n\n<br>";
				$message .= $this->language->get('text_status') . ': ' . $status[0]['name'] . "\n";

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get(	'config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($custmEmail[0]['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name')), ENT_QUOTES, 'UTF-8');
				$mail->setSubject(html_entity_decode($customer_email_info[0]['subject'], ENT_QUOTES, 'UTF-8'));
				$mail->sethtml($message);
				$mail->send();

			// Send to additional alert emails if new account email is enabled
				$emails = explode(',', $this->config->get('config_alert_email'));

				foreach ($emails as $email) {
					if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
						$mail->setTo($email);
						$mail->send();
						}
					}
			
			}
			
		
		
		
	}
	
	public function getname($logId){
	
	
	$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "user WHERE user_id = '".(int)$logId."'");
		
	return $query->rows;
	}	
	public function getStatusNotify($support_id){
	
	$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "support_status_histroy WHERE support_id = '".(int)$support_id."'ORDER BY notify_id DESC LIMIT 1");
		
	return $query->row;
	}
	
	
	public function addComment($support_id,$data,$name){
	
		if(!empty($data)){
				
	
			$sql= $this->db->query("INSERT INTO " . DB_PREFIX . "support_comment SET `comment` = '" .  $this->db->escape($data) . "',name='".$this->db->escape($name)."',date_added=NOW(),`status` ='2',support_id = '".(int)$support_id."'");
		
			$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "support s LEFT JOIN " . DB_PREFIX . "department d ON (s.department = d.department_id) LEFT JOIN " . DB_PREFIX . "customer c ON (s.customer_id = c.customer_id) WHERE s.support_id = '" . (int)$support_id. "'");
			
			$custmEmail=$customer->rows;
			//notify customer email	detail
			$email_Detail_admin=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_email WHERE notify = '2'");
			$customer_email_info=$email_Detail_admin->rows;
			
				
			
				//ticket detail
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "support s LEFT JOIN " . DB_PREFIX . "department d ON (s.department = d.department_id) LEFT JOIN " . DB_PREFIX . "customer c ON (s.customer_id = c.customer_id) WHERE s.support_id = '" . (int)$support_id. "'");
			
			$ticket_info=$query->rows;
			
			
		//status detail
		$status_Detail=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_status WHERE status_id = '".$ticket_info[0]['status']."'");
		$status_info=$status_Detail->rows;
				
			
			
			
	// Send to main admin email if new account email is enabled
	
	
			$message  = html_entity_decode($customer_email_info[0]['message'],ENT_QUOTES, 'UTF-8') . "\n\n<br>";
	
			$message .= '<html>';
			$message .= '<body>';
			$message .= '<table style=" border-collapse: collapse;
			width: 100%;">';
			
			
			$message .= "<tr><td style=' border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('support_id') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['support_id'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_subject') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['subject'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_department') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['department'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_status') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$status_info[0]['name'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_comment') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$data . "<td></tr>";
			$message .= '</table>';
			$message .= '</body>';
			$message .= '</html>';
			
			

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($custmEmail[0]['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name')), ENT_QUOTES, 'UTF-8');
			$mail->setSubject(html_entity_decode($customer_email_info[0]['subject'], ENT_QUOTES, 'UTF-8'));
				$mail->sethtml($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		return $comment_id;
			}	
	}
	

	
	public function deletedepartment($department_id) {
		
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "support  WHERE department = '" . (int)$department_id. "'");
			if(!empty($query->num_rows)){
				
				return 0;
				
			}
			else{
		
			$query=$this->db->query("DELETE FROM " . DB_PREFIX . "department WHERE department_id = '" . (int)$department_id . "'");
		
				return 1;
		
			}
	
	
		
	}
	public function deletestatus($status_id) {
		
			$query=$this->db->query("DELETE FROM " . DB_PREFIX . "support_status WHERE status_id = '" . (int)$status_id . "'");

	return $query;
	
		
	}
     public function deleteticket($support_id) {
	
		$this->db->query("DELETE  FROM " . DB_PREFIX . "support WHERE support_id = '" . (int)$support_id . "'");
		
		$this->db->query("DELETE  FROM " . DB_PREFIX . "support_comment WHERE support_id = '" . (int)$support_id . "'");
		
		$this->db->query("DELETE  FROM " . DB_PREFIX . "support_file WHERE support_id = '" . (int)$support_id . "'");
		
		$this->db->query("DELETE  FROM " . DB_PREFIX . "support_status_histroy WHERE support_id = '" . (int)$support_id . "'");
	
	}

	
	public function getDepartment($department_id) {
		
		$query = $this->db->query("SELECT department FROM ". DB_PREFIX . "department WHERE department_id = '".(int)$department_id."'");

		return $query->rows;
	}	
	public function getStatusName($status_id) {
		
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "support_status WHERE status_id = '".(int)$status_id."'");

	
		
		return $query->rows;
	}
	

		public function getCustomer($customer_id) {
		
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "customer WHERE customer_id = '".(int)$customer_id."'");

		return $query->rows;
	}
	
	public function getComment($support_id) {
		
		
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "support_comment WHERE support_id = '".(int)$support_id."'ORDER BY date_added ASC");

		return $query->rows;
	}
	
	
	public function getTicketsview($support_id) {
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "support s LEFT JOIN " . DB_PREFIX . "department d ON (s.department = d.department_id) LEFT JOIN " . DB_PREFIX . "customer c ON (s.customer_id = c.customer_id)LEFT JOIN " . DB_PREFIX . "support_status ss ON (s.status_id = ss.status_id) WHERE s.support_id = '" . (int)$support_id. "'");
		

		return $query->rows;
	}
	
	
	public function getSupportFile($support_id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "support_file  WHERE support_id = '" . (int)$support_id. "'");
		

		return $query->rows;
	}
	
	
	
	
	public function getTickets($data = array()) {
		
		
		$sql = "SELECT * FROM " . DB_PREFIX . "support s WHERE support_id != 0 ";
		
		if (isset($data['filter_department']) && !is_null($data['filter_department'])) {
			
			$sql .= " AND s.department = '" .(int)$data['filter_department']. "'";
		}
		
		if (isset($data['filter_date_added']) && !is_null($data['filter_date_added'])) {
			
			$sql .= " AND s.date_added = '" .$data['filter_date_added']. "'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			
			$sql .= " AND s.status_id = '" .(int)$data['filter_status']. "'";
			
		}
		
		
		$sql .= " order by support_id desc";
		
			if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}	
	public function getHistory($support_id) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "support_status_histroy  WHERE support_id ='".(int)$support_id."'";

		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getDepartments($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "department WHERE department_id != 0";

		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getstatus()
	{
		$sql = "SELECT * FROM " . DB_PREFIX . "support_status";

		$query = $this->db->query($sql);

		return $query->rows;
		
		
	}

	
	public function getTotalTickets($data = array()) {
		$sql = "SELECT COUNT(DISTINCT support_id) AS total FROM " . DB_PREFIX . "support ";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getTotalStatus($data = array()) {
		$sql = "SELECT COUNT(DISTINCT status_id) AS total FROM " . DB_PREFIX . "support_status ";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	
	
	public function getTotalHistory($data = array()) {
		$sql = "SELECT COUNT(DISTINCT support_id) AS total FROM " . DB_PREFIX . "support ";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	

	public function getTotalDepartment() {
		$sql = "SELECT COUNT(DISTINCT department_id) AS total FROM " . DB_PREFIX . "department ";


		$query = $this->db->query($sql);

		return $query->row['total'];
	}
		public function getDepartmentList() {
		$sql = "SELECT * FROM " . DB_PREFIX . "department ";


		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getemail() {
		$sql = "SELECT * FROM " . DB_PREFIX . "support_email ";


		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getModuleSetting($code)
	{
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "module` WHERE `code` = '" . $code . "'");

		if ($query->row) {
			return json_decode($query->row['setting'], true);
		} else {
			return array();
		}
		
	}
public function editSetting($code,$data)
	{
		$this->db->query("UPDATE `" . DB_PREFIX . "module` SET `name` = '" . $this->db->escape($data['name']) . "', `setting` = '" . $this->db->escape(json_encode($data)) . "' WHERE `code` = '" . $code . "'");
	}

}
