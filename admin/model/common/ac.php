<?php
class ModelCommonAc extends Model {
  //get ALl Quotes
  public function allQuote($data)
  {

    $sql = "SELECT * FROM product_quotes ORDER BY date_added ";

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    
    }

    $results = $this->db->query($sql);

    $quotes = array();
    foreach($results->rows as $result){
      $quotes[] = array(
        'id' => $result['id'],
        'full_name' => $result['full_name'],
        'phone' => $result['phone'],
        'email' => $result['email'],
        'product_data' => json_decode(html_entity_decode($result['product_data'])),
        'date_added' => date("d M, Y",strtotime($result['date_added'])),
        'answered' => (int)$result['answered'],
        'href'  => HTTPS_CATALOG.'index.php?route=product/product&product_id=',
      );
    }

    return $quotes;
  }

  public function quoteTotal(){
    
    $sql = "SELECT COUNT(*) as total FROM product_quotes ";

    return $this->db->query($sql)->row['total'];

  }

}