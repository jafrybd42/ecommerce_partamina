<?php
class ModelExtensionModuleSupport extends Model {
	public function getDepartment() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "department");

		return $query->rows;
	}
		public function getGroups() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_group ORDER BY sort_order ASC ;" );

		return $query->rows;
	}

	
		public function getDescriptions($group_id){
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_support WHERE group_id = '" . (int)$group_id . "'AND  language_id='" . (int)$this->config->get('config_language_id') . "' ");
		
		return $query->rows;
	}
		
		public function getTickets($data = array(),$logId) {
		$sql = "SELECT * FROM " . DB_PREFIX . "support s WHERE customer_id = '".(int)$logId."'order by support_id desc";
		
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		
			$query = $this->db->query($sql);

		return $query->rows;
	
		
	
		}
        public function getTicketsview($support_id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "support s LEFT JOIN " . DB_PREFIX . "department d ON (s.department = d.department_id) LEFT JOIN " . DB_PREFIX . "customer c ON (s.customer_id = c.customer_id)LEFT JOIN " . DB_PREFIX . "support_status ss ON (s.status_id = ss.status_id) WHERE s.support_id = '" . (int)$support_id. "'");
		

		return $query->rows;
	     }
	
	    public function getStatusName($status_id) {
		
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "support_status WHERE status_id = '".(int)$status_id."'");

		return $query->rows;
	   }


		public function getCustomer($customer_id) {
		
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "customer WHERE customer_id = '".(int)$customer_id."'");

		return $query->rows;
	    }
	
	    public function getComment($support_id) {
		
		
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "support_comment WHERE support_id = '".(int)$support_id."'");

		return $query->rows;
	  }
	
		public function getSupportFile($support_id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "support_file  WHERE support_id = '" . (int)$support_id. "'");
		

		return $query->rows;
	   }
	
	    public function getTotalTickets($data = array(),$logId) {
		
		$sql = "SELECT COUNT(DISTINCT support_id) AS total FROM " . DB_PREFIX . "support WHERE customer_id = '".(int)$logId."'";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

        public function getDepartments($data = array()) {
		  $sql = "SELECT * FROM " . DB_PREFIX . "department WHERE department_id != 0";

		  $query = $this->db->query($sql);

		return $query->rows;
	   }



	     public function getGeneral(){

		$group_id = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_group ORDER BY sort_order ASC ,group_id ASC LIMIT 1");
		
		$group= $group_id->row;
		if(!empty( $group)){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "faq_support WHERE group_id = '".(int)$group['group_id']."' AND  language_id='" . (int)$this->config->get('config_language_id') . "' ");
			
		return $query->rows;
		}
		
	     }
	   public function addCustomerReport($data,$customer_id){
		
		
		$sql= $this->db->query("INSERT INTO " . DB_PREFIX . "support SET `subject` = '" . $this->db->escape($data['subject']) . "',`department` = '" . (int)$data['choose_department_id'] . "',`message` = '" . $this->db->escape($data['message']) . "',`customer_id` = '" . (int)$customer_id . "',`status_id` = '2',date_added = NOW()");
			
    	$support_id = $this->db->getLastId();
		
		
		$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id. "'");
			$custmEmail=$customer->rows;
			
			$email_Detail=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_email WHERE flag = '2'");
			$email_info=$email_Detail->rows;
			
			$email_Detail_admin=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_email WHERE flag = '1'");
			$admin_info=$email_Detail_admin->rows;
			
	// Send to main admin email if new account email is enabled
		 	 $message  = html_entity_decode($admin_info[0]['message'],ENT_QUOTES, 'UTF-8') . "\n\n";
			$message .= $this->language->get('entry_subject') . ': ' . $data['subject'] . "\n\n<br>";
			$message .= $this->language->get('entry_message') . ': ' . $data['message'] . "\n";
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($custmEmail[0]['email']);
		
			$mail->setSender(html_entity_decode($this->config->get('config_name')), ENT_QUOTES, 'UTF-8');
			$mail->setSubject($admin_info[0]['subject']);
			$mail->sethtml($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
			
			
			//customer
			 $message  = html_entity_decode($email_info[0]['message'],ENT_QUOTES, 'UTF-8') . "\n\n";
			$message .= $this->language->get('entry_subject') . ': ' . $data['subject'] . "\n\n<br>";
			$message .= $this->language->get('entry_message') . ': ' . $data['message'] . "\n";
			
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			
			$mail->setTo($custmEmail[0]['email']);
			$mail->setFrom($this->config->get('config_email'));
			
			$mail->setSender(html_entity_decode($this->config->get('config_name')), ENT_QUOTES, 'UTF-8');
			$mail->setSubject($email_info[0]['subject']);
			$mail->sethtml($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
			
			
		
	
		
		return $support_id;
		
	  }

	  public function getname($logId){
	
	
	  $query = $this->db->query("SELECT * FROM ". DB_PREFIX . "user WHERE user_id = '".(int)$logId."'");
		
	  return $query->rows;
	  }
	
	   public function addComment($support_id,$data,$name){
		
			if(!empty($data['comment'])){
	
		$sql= $this->db->query("INSERT INTO " . DB_PREFIX . "support_comment SET `comment` = '" .  $this->db->escape($data['comment']) . "',name='".$this->db->escape($name)."',status = '1',support_id = '".(int)$support_id."',date_added = NOW()");
		
		//customer detail
		$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "support s LEFT JOIN " . DB_PREFIX . "department d ON (s.department = d.department_id) LEFT JOIN " . DB_PREFIX . "customer c ON (s.customer_id = c.customer_id) WHERE s.support_id = '" . (int)$support_id. "'");
			
		$custmEmail=$customer->rows;
			
			//ticket detail
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "support s LEFT JOIN " . DB_PREFIX . "department d ON (s.department = d.department_id) LEFT JOIN " . DB_PREFIX . "customer c ON (s.customer_id = c.customer_id) WHERE s.support_id = '" . (int)$support_id. "'");
			
			$ticket_info=$query->rows;
			
			
		//status detail
		$status_Detail=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_status WHERE status_id = '".$ticket_info[0]['status']."'");
		$status_info=$status_Detail->rows;
				
		//email detail
		$email_Detail=$this->db->query("SELECT * FROM " . DB_PREFIX . "support_email WHERE notify = '2'");
		$email_info=$email_Detail->rows;
			
			
			
			
			
			
			
			
	// Send to main admin email if new account email is enabled
			$message = '<html>';
			$message .= '<body>';
			$message .= '<table style=" border-collapse: collapse;
    width: 100%;">';
			
			
			$message .= "<tr><td style=' border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('support_id') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['support_id'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_subject') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['subject'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_department') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['department'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_status') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$status_info[0]['name'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_comment') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$data['comment'] . "<td></tr>";
			$message .= '</table>';
			$message .= '</body>';
			$message .= '</html>';
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($custmEmail[0]['email']);
			$mail->setSender(html_entity_decode($this->config->get('config_name')), ENT_QUOTES, 'UTF-8');
			$mail->setSubject($this->language->get('text_comment_subject'));
			$mail->sethtml($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		
		//customer
			 $message  = html_entity_decode($email_info[0]['message'],ENT_QUOTES, 'UTF-8') . "\n\n";
			$message = '<html>';
			$message .= '<body>';
			$message .= '<table style=" border-collapse: collapse;
    width: 100%;">';
			
			
			$message .= "<tr><td style=' border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('support_id') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['support_id'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_subject') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['subject'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_department') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$ticket_info[0]['department'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_status') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$status_info[0]['name'] . "<td></tr>";
			$message .= "<tr><td style='border: 1px solid black;height:30px;  padding: 6px;'>" .$this->language->get('entry_comment') . '</td><td style="border: 1px solid black;height:30px;  padding: 6px;"> '.$data['comment'] . "<td></tr>";
			$message .= '</table>';
			$message .= '</body>';
			$message .= '</html>';
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			
			$mail->setTo($custmEmail[0]['email']);
			$mail->setFrom($this->config->get('config_email'));
			
			$mail->setSender(html_entity_decode($this->config->get('config_name')), ENT_QUOTES, 'UTF-8');
			$mail->setSubject($email_info[0]['subject']);
			$mail->sethtml($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		
		
		
		
		
		//return $comment_id;
			}	
	 }
	 public function getstatus()
	 {
		$sql = "SELECT * FROM " . DB_PREFIX . "support_status";

		$query = $this->db->query($sql);

		return $query->rows;
		
		
	}
	

	public function addFile($fileext,$support_id){
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "support_file SET file_name = '" . $this->db->escape( $fileext ). "',support_id = '" . (int)$support_id . "'"); 
		
	}
	 
 public function getModuleSetting($code)
	{
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "module` WHERE `code` = '" . $code . "'");

		if ($query->row) {
			return json_decode($query->row['setting'], true);
		} else {
			return array();
		}
		
	}
	public function editSetting($code,$data)
	{
		$this->db->query("UPDATE `" . DB_PREFIX . "module` SET `name` = '" . $this->db->escape($data['name']) . "', `setting` = '" . $this->db->escape(json_encode($data)) . "' WHERE `code` = '" . $code . "'");
	}
}