<?php 
/**
 * Order.php
 *
 * Order method management 
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 * order_staus_id info
 * 		1 = Order placed
 * 		2 = Order Processing
 * 		15 = Order Confirmed
 * 		17 = Order in transit
 * 		
 * 		5 = Order Delivered
 * 		7 = Canceled
 * 		10 = Failed
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiOrder extends RestApiController {

	public function list()
	{
    
    $this->auth('post' );

    $post = $this->getPost();
		
    $page = $post['page'];
		$limit = $post['limit'];
		
		if(isset($post['filter']) && $post['filter']){
			$filter = $post['filter'];
			if($filter == "to_be_delivered"){
				$filter = '1,2,3,15,17,18';
			}elseif($filter == "history"){
				$filter = '5,7,8,9,10,11,12,13,14,16';
			}else{
				$this->statusCode = 400;
				$this->json["error"][] = $this->language->get("invalid_filter");
			}
		}else{
			$filter = '';
		}

		if(!$this->json["error"]){

			$order_total = $this->model_ac_api_common->getTotalOrders($filter);
			$results = $this->model_ac_api_common->getOrders(($page - 1) * $limit, $limit, $filter);
			
			if($order_total){
	
				$data['orders'] = $results;

				//Pagination
				$total_pages = ceil($order_total / $limit);
				$next_page = $page+1;

				$data['pagination'] = array(
					'order_total' => $order_total,
					'total_pages'   => $total_pages,
					'current'       => $page,
					'next'          => ($next_page <= $total_pages)?$next_page:0,
					'limit'         => $limit
				);
				
				$this->json['data'] = $data;

			}else{
				$this->statusCode = 404;
      	$this->json["error"][] = $this->language->get("no_order_found");
			}
		
    }

    $this->sendResponse();
	}
	
	public function detail() 
	{

    $this->auth('post');

    $post = $this->getPost();
    $this->validateData(['order_id'],$post);

    $this->model('account/order');

    $order_info = $this->model_account_order->getOrder($post['order_id']);
    if($order_info){
      
      $this->model('catalog/product');
      $this->model('tool/upload');
      
      $data['order_statuses'] = $this->model_ac_api_common->getOrderStatuses([1,2,15,17,5]);
      $data['order_status_id'] = $order_info['order_status_id'];

      $data['order_id'] = $post['order_id'];
      $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
      
      //Payment address
      if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
      }
      $find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
      );
      
      $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), ',', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), ',', trim(str_replace($find, $replace, $format))));
      //payment adderess end
      $data['payment_method'] = $order_info['payment_method'];

      //shipping adderess
      if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
      }
      
      $find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), ', ', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), ', ', trim(str_replace($find, $replace, $format))));

			$data['shipping_method'] = $order_info['shipping_method'];
 //shipping adderess end

      //Eestimated Delivery time
      $data['estimated_time'] =  "Sat, June 7";

      $data['comment'] = nl2br($order_info['comment']);

      // Products
      $data['products'] = array();

			$products = $this->model_account_order->getOrderProducts($post['order_id']);
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($post['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}
				
				$image = $this->model_ac_api_common->getProductImage($product['product_id']);

				$data['products'][] = array(
					'product_id' 	=> $product['product_id'],
					'name'     		=> $product['name'],
					'image'				=> $this->imageLink($image),
					'model'    		=> $product['model'],
					'option'   		=> $option_data,
					'quantity' 		=> $product['quantity'],
					'price'    		=> $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    		=> $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
				);
      }

      $data['total_item'] = count($data['products']);
      
      // Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($post['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
      }

      // Totals
			$data['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($post['order_id']);

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
      }

      // History
			$data['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($post['order_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
      }
      

      $this->json["data"] = $data;
      
    }else{
      $this->statusCode = 404;
      $this->json["error"][] = $this->language->get('order_not_valid');
    }

    $this->sendResponse();
	}  

	public function delivery_status()
	{
	
    $this->auth('post');
		$post = $this->getPost();

		$this->validateData(['order_id'],$post);
		
		$order_id = $post['order_id'];

		$delivery_history = $this->model_ac_api_common->getOrderDeliveryHistory($order_id);

		if($delivery_history){
			$this->json["data"]['current_status'] = $delivery_history[0];
			$this->json["data"]['history'] = $delivery_history;
		}else{
			$this->statusCode = 404;
      $this->json["error"][] = $this->language->get('order_not_valid');	
		}

		$this->sendResponse();	
	}

}