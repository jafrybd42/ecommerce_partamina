<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_storeLocation']  = 'Bashundhara, Dhaka';
$_['text_storeAddress']     = 'Petro Products Ltd.';
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';
$_['button_submit']     = 'Submit';
// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';


//subject question
$_['text_subject']  = 'Select Subject';
$_['text_orderStatus']  = 'Order status and delivery deadline';
$_['text_trackOrder']  = 'How can I track my order?  ';
$_['text_orderRecive']  = 'How long it takes to restore order?';
$_['text_OrderNotFound']  = 'My order is late. What should I do?';
$_['text_orderLate']  = 'Why is my order late? ';
$_['text_orderDelivery']  = 'How long will it take for my order to be delivered? ';
$_['text_orderReturn']  = 'Return and refund ';
$_['text_orderReturning']  = 'How can I get my product back? ';
$_['text_orderNotReturned']  = 'I have not been returned yet ';
$_['text_orderRefund']  = 'What is the return policy? ';

$_['text_orderGetRefund']  = 'When can I get my money back? ';
$_['text_orderRefundProcess']  = 'What is the refund process? ';
$_['text_orderPlacement']  = 'Order placement ';
$_['text_orderHowToPlace']  = 'How to place an order? ';
$_['text_orderHowToPay']  = 'How do I pay? ';
$_['text_orderCancel']  = 'How do I cancel my order? ';
$_['text_orderplace']  = 'What is the way to place an order? ';
$_['text_ordercancel']  = 'What is the way to cancel an order? ';

