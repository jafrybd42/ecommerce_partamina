<?php
// Heading
$_['heading_title'] = 'Your Request Has Been Submitted!';

// Text
$_['text_message']  = '<p> Your request has been successfully submitted!</p> <p>We will get back to you soon!</p>'; 
$_['text_account']  = 'Account';
$_['text_success']  = 'Success';
