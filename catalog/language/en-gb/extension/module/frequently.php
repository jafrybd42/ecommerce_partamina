<?php
// Heading
$_['heading_title']        = 'FAQ';

// Text
$_['text_account']         = 'Account';
$_['text_support']        = 'FAQ';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'FAQ';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['text_open_new']        = 'Open a new ticket';
$_['text_view_ticket']        = 'View Ticket List';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_subject']      = 'Subject';
$_['entry_choose_department']      = 'Choose Department';
$_['entry_message']          = 'Message';
$_['entry_file']      = 'Attachments';
$_['entry_confirm']        = 'Password Confirm';
