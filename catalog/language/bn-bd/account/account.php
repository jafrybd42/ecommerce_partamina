<?php
// Heading 
$_['heading_title']      = 'আমার অ্যাকাউন্ট';

// Text
$_['text_account']       = 'অ্যাকাউন্ট';
$_['text_my_account']    = 'আমার অ্যাকাউন্ট';
$_['text_my_orders']     = 'আমার অর্ডার সমূহ';
$_['text_my_newsletter'] = 'নিউজলেটার';
$_['text_edit']          = ' অ্যাকাউন্ট তথ্য পরিবর্তন করুন';
$_['text_password']      = ' পাসওয়ার্ড পরিবর্তন করুন';
$_['text_address']       = ' ঠিকানা পরিবর্তন করুন';
$_['text_wishlist']      = ' পছন্দের তালিকা পরিবর্তন করুন';
$_['text_order']         = ' পূর্বের অর্ডার সমূহ দেখুন';
$_['text_download']      = 'ডাউনলোড';
$_['text_reward']        = ' পুরস্কার পয়েন্ট'; 
$_['text_return']        = ' ফিরতি অনুরোধ দেখুন'; 
$_['text_transaction']   = ' লেনদেন'; 
$_['text_newsletter']    = 'সদস্যপদ / নিউজলেটার সদস্যপদ ত্যাগ';
$_['text_recurring']     = 'আবর্তক পেমেন্ট';
$_['text_transactions']  = 'লেনদেন';
?>