<?php
class ControllerApiCustomCart extends Controller {
    
    public function edit() {
        $json = array();
        
        if (!isset($this->session->data['api_id'])) {
            
    		$json['success'] = false;
    		$json['message'] = $this->language->get('error_permission');
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
        
        try {
            $customer_id = $this->request->post['customer_id'];
            $product_id = $this->request->post['product_id'];
            $quantity = $this->request->post['quantity'];
            
    		$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '" . (int)$quantity . "' WHERE product_id = '" . (int)$product_id . "' AND customer_id = '" . (int)$customer_id . "'");
    		
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
    		$json['data'] = array();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        catch (Exception $e) {
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
		
		$json =array();
		$json['success'] = false;
		$json['message'] = "unknown error";
	
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
	}

	public function remove() {
	    $json = array();
        
        if (!isset($this->session->data['api_id'])) {
            
			$json['success'] = false;
    		$json['message'] = $this->language->get('error_permission');
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
		
		try {
		    
            $product_id = $this->request->post['product_id'];
            $customer_id = $this->request->post['customer_id'];
        	$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE product_id = '" . (int)$product_id . "' AND customer_id = '" . (int)$customer_id . "'");
		
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
    		$json['data'] = array();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        catch (Exception $e) {
            
            $json =array();
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
		
		$json =array();
		$json['success'] = false;
		$json['message'] = "unknown error";
	
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
	}

	public function clear() {
	    $json = array();
        
        if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
			$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));
		}
		
	    $customer_id = $this->request->post['customer_id'];
	    $this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$customer_id . "'");
	    
	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode("success"));
	}
	
    public function index() {
        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($this->session));
    }
    
	public function add() {
		
		$this->load->language('api/cart');

		$json = array();
		
	   /*echo "<pre>";
		print_r([$this->customer->getId(),$this->session->getId(),$this->session->data['api_id'],$this->request->post['customer_id']]);
		echo "</pre>";
		die('dieeee...');exit;*/
			
		if (!isset($this->session->data['api_id'])) {

    		$json['success'] = false;
    		$json['message'] = $this->language->get('error_permission');
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		} 
		else {
		    
		    /*if( (int)$this->customer->getId() == 0 ){
		        
		        $json['success'] = false;
        		$json['message'] = 'Api session error. Please login again ';
    		
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
		        
		    }*/
		    
		
// 			if (isset($this->request->post['product'])) {
// 				$this->cart->clear();

// 				foreach ($this->request->post['product'] as $product) {
// 					if (isset($product['option'])) {
// 						$option = $product['option'];
// 					} else {
// 						$option = array();
// 					}

// 					$this->cart->add($product['product_id'], $product['quantity'], $option);
// 				}

// 				$json['success'] = $this->language->get('text_success');

// 				unset($this->session->data['shipping_method']);
// 				unset($this->session->data['shipping_methods']);
// 				unset($this->session->data['payment_method']);
// 				unset($this->session->data['payment_methods']);
// 			} 
			
			if (isset($this->request->post['product_id'])) {
				$this->load->model('catalog/product');
				
				try {

    				$product_info = $this->model_catalog_product->getProduct($this->request->post['product_id']);
    
    				if ($product_info) {
    				    
    					if (isset($this->request->post['quantity'])) {
    						$quantity = $this->request->post['quantity'];
    					} else {
    						$quantity = 1;
    					}
    
    					if (isset($this->request->post['option'])) {
    						$option = array_filter($this->request->post['option']);
    					} else {
    						$option = array();
    					}
    
    					$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
    
    					foreach ($product_options as $product_option) {
    						if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
    							$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
    						}
    					}
    
    					if (!isset($json['error']['option'])) {
    					    
    					    $customerID = $this->request->post['customer_id'];
    					    $productID = $this->request->post['product_id'];
    					    $quantity = $this->request->post['quantity'];
    					    $recurring_id = 0;
    					    
    						$this->cart->add($this->request->post['product_id'], $quantity, $option);
    						
    						//custom cart function
    						//$this->addProductToCart($this->request->post['product_id'], $quantity, $option,$customerID);
    						
    					    //	$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$customerID . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
    
    
                           /* if (!$cart_query->num_rows) {
                    			$this->db->query("INSERT " . DB_PREFIX . "cart SET api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "', customer_id = '" . (int)$customerID . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$productID . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
                    		} else {
                    			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . ") WHERE api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$customerID . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$productID . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
                    		}*/
    
    
                    		$json['success'] = true;
                    		$json['message'] = "The request is successful";
                    		
                    		$json['data'] = array();
                    		
                    		$this->response->addHeader('Content-Type: application/json');
                            $this->response->setOutput(json_encode($json));
                            return;
    
    						/*unset($this->session->data['shipping_method']);
    						unset($this->session->data['shipping_methods']);
    						unset($this->session->data['payment_method']);
    						unset($this->session->data['payment_methods']);*/
    					}
    				} 
    				else {
    				    
                		$json['success'] = false;
                		$json['message'] = $this->language->get('error_store');
            		
                        $this->response->addHeader('Content-Type: application/json');
                        $this->response->setOutput(json_encode($json));
                        return;
    				}
				}
				catch (Exception $e) {
                    $json =array();
            		$json['success'] = false;
            		$json['message'] = $e->getMessage();
        		
                    $this->response->addHeader('Content-Type: application/json');
                    $this->response->setOutput(json_encode($json));
                    return;
                }
			}
		}

		$json['success'] = false;
		$json['message'] = "unknown error";
	
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
	}
	
	

	/*public function edit() {
		$this->load->language('api/cart');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$this->cart->update($this->request->post['key'], $this->request->post['quantity']);

			$json['success'] = $this->language->get('text_success');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}*/

	/*public function remove() {
		$this->load->language('api/cart');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			// Remove
			if (isset($this->request->post['key'])) {
				$this->cart->remove($this->request->post['key']);

				unset($this->session->data['vouchers'][$this->request->post['key']]);

				$json['success'] = $this->language->get('text_success');

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['reward']);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}*/

	public function products() {
		$this->load->language('api/cart');

		$json = array();
		
		/*echo "<pre>";
		print_r($this->cart->hasShipping());
		echo "</pre>";
		die("die...");exit;*/

		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {
			// Stock
			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$json['error']['stock'] = $this->language->get('error_stock');
			}

			// Products
			$json['products'] = array();

			//$products = $this->cart->getProducts($this->request->get['customer_id']);
			$products = $this->cart->getProducts();

			foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$json['error']['minimum'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				$option_data = array();

				foreach ($product['option'] as $option) {
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				$json['products'][] = array(
					'cart_id'    => $product['cart_id'],
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $option_data,
					'quantity'   => $product['quantity'],
					'stock'      => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'shipping'   => $product['shipping'],
					'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
					'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']),
					'reward'     => $product['reward']
				);
			}

			// Voucher
			$json['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$json['vouchers'][] = array(
						'code'             => $voucher['code'],
						'description'      => $voucher['description'],
						'from_name'        => $voucher['from_name'],
						'from_email'       => $voucher['from_email'],
						'to_name'          => $voucher['to_name'],
						'to_email'         => $voucher['to_email'],
						'voucher_theme_id' => $voucher['voucher_theme_id'],
						'message'          => $voucher['message'],
						'price'            => $this->currency->format($voucher['amount'], $this->session->data['currency']),			
						'amount'           => $voucher['amount']
					);
				}
			}

			// Totals
			$this->load->model('setting/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			$sort_order = array();

			$results = $this->model_setting_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get('total_' . $result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);
					
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);

			$json['totals'] = array();

			foreach ($totals as $total) {
				$json['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
}
