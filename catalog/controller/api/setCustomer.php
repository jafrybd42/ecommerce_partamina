<?php
class ControllerApiSetCustomer extends Controller {
    
    public function index() {
        if(isset($this->request->post['customer_id'])) {
            $this->session->data['customer_id'] = $this->request->post['customer_id'];
		    
            $this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($this->session));
		
		    return;
        }
        else {
            $this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode("customer id is required"));
		    return;
        }
        
        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode("unknown error"));
    }
}