<?php
class ControllerApiGetStaticContents extends Controller {
    
    public function getContent($info_id, $lang_id = 3) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_description WHERE information_id = '" . (int)$info_id . "' AND language_id = '" . (int)$lang_id . "' ");
		return $query->rows;
	}
	
	public function index() {
	    $info_id = $this->request->post['info_id'];
	    $json =array();
        $result = $this->getContent($info_id);
        
        $hasResults = 0;
        
        foreach ($result as $results) {
            $hasResults += 1;
			$data['info'][] = array(
				'title' => $results['title'],
				'description'       => $results['description']
			);
		}
        
        if($hasResults > 0) {
    	    $json['success'] = "True";
            $json['message'] = "The request is successful";
    	    $json['data']['info'] = $data['info'];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
    	}
    	else {
    	    $json['success'] = "True";
            $json['message'] = "no data found";
    	    $json['data'] = array();
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
    	}
    	$json['success'] = "False";
    	$json['message'] = "unknown error";
    	$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
	}
	
	/*
	public function faq() {
	    $info_id = $this->request->post['$info_id'];
	    $json =array();
        $result = $this->getContent($info_id);
        
        foreach ($result as $results) {
    			$data['info'][] = array(
    				'title' => $results['title'],
    				'description'       => $results['description']
    			);
    		}
            $json['info'] = $data['info'];
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function privacypolicy() {

	    $json =array();
        
        // $this->load->model('design/banner');
        $result = $this->getBanners();
        
        foreach ($result as $results) {
    			$data['banners'][] = array(
    				'id'     => $results['banner_image_id'],
    				'title' => $results['title'],
    				'link'       => $results['link'],
    				'image'     => $results['image']
    			);
    		}
            $json['banners'] = $data['banners'];
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function inquiryform() {

	    $json =array();
        
        // $this->load->model('design/banner');
        $result = $this->getBanners();
        
        foreach ($result as $results) {
    			$data['banners'][] = array(
    				'id'     => $results['banner_image_id'],
    				'title' => $results['title'],
    				'link'       => $results['link'],
    				'image'     => $results['image']
    			);
    		}
            $json['banners'] = $data['banners'];
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function termsconditions() {

	    $json =array();
        
        // $this->load->model('design/banner');
        $result = $this->getBanners();
        
        foreach ($result as $results) {
    			$data['banners'][] = array(
    				'id'     => $results['banner_image_id'],
    				'title' => $results['title'],
    				'link'       => $results['link'],
    				'image'     => $results['image']
    			);
    		}
            $json['banners'] = $data['banners'];
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function deliveryinfo() {

	    $json =array();
        
        // $this->load->model('design/banner');
        $result = $this->getBanners();
        
        foreach ($result as $results) {
    			$data['banners'][] = array(
    				'id'     => $results['banner_image_id'],
    				'title' => $results['title'],
    				'link'       => $results['link'],
    				'image'     => $results['image']
    			);
    		}
            $json['banners'] = $data['banners'];
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	*/
}