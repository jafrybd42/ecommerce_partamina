<?php
class ControllerApiCancelOrder extends Controller {
    
    public function changeOrderStatus($o_id) {
        
        $status = 0;
        $order_id = $o_id;
        
		$query = $this->db->query("UPDATE " . DB_PREFIX . "order SET status = '" . (int)$status . "' WHERE order_id = '" . (int)$order_id . "'");
		
		return $query;
	}
	
	public function index() {
	    
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode("dummy response"));
        return;
        
	    $json =array();
	    
	    try {
    	    $order_id = $this->request->post['order_id'];
            
            $result = $this->changeOrderStatus($order_id);
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
    		
    		$json['data'] = array();
    		
    		$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));
		    return;
        }
        catch (Exception $e) {
	        
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $json['success'] = false;
		$json['message'] = "unknown error occured";
        		
        $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
	}
}