<?php
class ControllerApiProductOffer extends Controller {
    
    public function getProductDiscounts() {
        
        $json =array();
        // $customer_group_id = $this->request->post['customer_group_id'];
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");

		$result = $query->rows;
		
		$hasResults = 0;
		
		foreach ($result as $results) {
		    $hasResults += 1;
    			$data['discounts'][] = array(
    				'product_discount_id'     => $results['product_discount_id'],
    				'customer_group_id' => $results['customer_group_id'],
    				'quantity'       => $results['quantity'],
    				'priority'     => $results['priority'],
    				'price'     => $results['price'],
    				'date_start'     => $results['date_start'],
    				'date_end'     => $results['date_end']
    			);
    		}
    	if($hasResults > 0) {
    	    $json['success'] = "True";
            $json['message'] = "The request is successful";
    	    $json['data']['discounts'] = $data['discounts'];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
    	}
    	else {
    	    $json['success'] = "True";
            $json['message'] = "no data found";
    	    $json['data'] = array();
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
    	}
    	$json['success'] = "False";
    	$json['message'] = "unknown error";
    	$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
	}
	
	public function getProductSpecials() {
	    
	    $json =array();
        // $customer_group_id = $this->request->post['customer_group_id'];
        
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC");

		$result = $query->rows;
		
		$hasResults = 0;
		
		foreach ($result as $results) {
		    $hasResults += 1;
    			$data['specials'][] = array(
    				'product_special_id'     => $results['product_special_id'],
    				'product_id'     => $results['product_id'],
    				'customer_group_id' => $results['customer_group_id'],
    				'priority'     => $results['priority'],
    				'price'     => $results['price'],
    				'date_start'     => $results['date_start'],
    				'date_end'     => $results['date_end']
    			);
    		}
        
        if($hasResults > 0) {
            $json['success'] = "True";
            $json['message'] = "The request is successful";
    	    $json['specials'] = $data['specials'];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
    	}
    	$json['success'] = "False";
    	$json['message'] = "no data found";
    	$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        return;
	}
	
	public function getBestSellerProducts() {
	    
		$product_data = array();

		$query = $this->db->query("SELECT op.product_id, SUM(op.quantity) AS total FROM oc_order_product op LEFT JOIN `oc_order` o ON (op.order_id = o.order_id) LEFT JOIN `oc_product` p ON (op.product_id = p.product_id) LEFT JOIN oc_product_to_store p2s ON (p.product_id = p2s.product_id) WHERE o.order_status_id > '0' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '0' GROUP BY op.product_id ORDER BY total DESC limit 10");
		
		if($query->num_rows > 0) {
		    $json =array();
    		$json['success'] = true;
    		$json['message'] = "The request is successful";
		
		    $json['data'] = $query->rows;
		} else {
		    $json =array();
    		$json['success'] = false;
    		$json['message'] = "no data found";
		}
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
		
	}
}