<?php
class ControllerApiEditPassword extends Controller {
    
    public function editPassword($email, $password) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
    
    public function index(){
        if(!isset($this->request->post['email']) && !isset($this->request->post['password']))
        {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode("email is requied"));
            return;
        }
        $email = $this->request->post['email'];
        $password = $this->request->post['password'];
        
        try {
            $result = $this->editPassword($email, $password);
            
            $json['success'] = true;
    		$json['message'] = "The request is successful";
    		
    		$json['data'] = array();
    		
    		$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));
		    return;
        }
        catch (Exception $e) {
	        
    		$json['success'] = false;
    		$json['message'] = $e->getMessage();
		
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $json['success'] = false;
		$json['message'] = "unknown error occured";
        		
        $this->response->addHeader('Content-Type: application/json');
	    $this->response->setOutput(json_encode($json));
    }
    
}
