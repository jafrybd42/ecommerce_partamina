<?php 
/**
 * address.php
 *
 * Address management
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiAddress extends RestApiController {
  
  private $error = array();

  function list()
  {
    $this->needLogin();
    $this->auth('get');
    $this->model('account/address');

    $this->json['data'] = array_values($this->model_account_address->getAddresses());
    
    $this->sendResponse();
  }

  function get()
  {
    $this->needLogin();
    $this->auth('POST');
    $this->model('account/address');

    $post = $this->getPost();
    $this->validateData(['address_id'],$post);

    $address = $this->model_account_address->getAddress($post['address_id']);

    if($address){
      $this->json['data'] = $address;
    }else{
      $this->json['error'][] = $this->language->get('text_address_not_found');
    }
    
    $this->sendResponse();
  }

  function update()
  {
    $this->needLogin();
    $this->auth('POST');
    $this->model('account/address');

    $post = $this->getPost();
    $this->validateData(['address_id','firstname','lastname','company','address_1','address_2','city','postcode','country_id','zone_id','default'],$post);

    if($this->validateAddressData($post)){
      
      $this->model_account_address->editAddress($post['address_id'], $post);
      $this->json['data']['message'] = $this->language->get('text_address_update_success');

    }else{
      $this->json['error'] = $this->error;
    }

    $this->sendResponse();
  }

  function add()
  {
    $this->needLogin();
    $this->auth('POST');
    $this->model('account/address');

    $post = $this->getPost();
    $this->validateData(['firstname','lastname','company','address_1','address_2','city','postcode','country_id','zone_id','default'],$post);

    if($this->validateAddressData($post)){
      
      $this->model_account_address->addAddress($this->customer->getId(), $post);

      //Set Default Address in session
      $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
      $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());

      $this->json['data']['message'] = $this->language->get('text_address_add_success');

    }else{
      $this->json['error'] = $this->error;
    }

    $this->sendResponse();
  }

  protected function validateAddressData($post) 
  {
    $this->load->language('account/address');

		if ((utf8_strlen(trim($post['firstname'])) < 1) || (utf8_strlen(trim($post['firstname'])) > 32)) {
			$this->error[] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($post['lastname'])) < 1) || (utf8_strlen(trim($post['lastname'])) > 32)) {
			$this->error[] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen(trim($post['address_1'])) < 3) || (utf8_strlen(trim($post['address_1'])) > 128)) {
			$this->error[] = $this->language->get('error_address_1');
		}

		if ((utf8_strlen(trim($post['city'])) < 2) || (utf8_strlen(trim($post['city'])) > 128)) {
			$this->error[] = $this->language->get('error_city');
		}

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($post['country_id']);

		if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($post['postcode'])) < 2 || utf8_strlen(trim($post['postcode'])) > 10)) {
			$this->error[] = $this->language->get('error_postcode');
		}

		if ($post['country_id'] == '' || !is_numeric($post['country_id'])) {
			$this->error[] = $this->language->get('error_country');
		}

		if (!isset($post['zone_id']) || $post['zone_id'] == '' || !is_numeric($post['zone_id'])) {
			$this->error[] = $this->language->get('error_zone');
		}

		return !$this->error;
  }
  

  public function delete() 
  {
    $this->needLogin();
    $this->auth('POST');
    $this->load->language('ac_api/account');
    $this->model('account/address');

    $post = $this->getPost();
    $this->validateData(['address_id'],$post);

		$this->load->language('account/address');
		$this->model('account/address');

    if($this->customer->getAddressId() == $post['address_id']){
      $this->json['error'][] = $this->language->get('error_default_address');
      $this->sendResponse();
    }

    $this->model_account_address->deleteAddress($post['address_id']);

    // Default Shipping Address
    if (isset($this->session->data['shipping_address']['address_id']) && ($post['address_id'] == $this->session->data['shipping_address']['address_id'])) {
      unset($this->session->data['shipping_address']);
      unset($this->session->data['shipping_method']);
      unset($this->session->data['shipping_methods']);
    }

    // Default Payment Address
    if (isset($this->session->data['payment_address']['address_id']) && ($post['address_id'] == $this->session->data['payment_address']['address_id'])) {
      unset($this->session->data['payment_address']);
      unset($this->session->data['payment_method']);
      unset($this->session->data['payment_methods']);
    }

    $this->json['data']['message'] = $this->language->get('address_text_delete');
     
    $this->sendResponse();
  }
  
  public function setDefault() 
  {
    $this->needLogin();
    $this->auth('POST');

    $post = $this->getPost();
    $this->validateData(['address_id'],$post);

		$this->model('account/customer');

    $this->model_account_customer->editAddressId($this->customer->getId(), $post['address_id']);

    $this->load->model('account/address');
    
    unset($this->session->data['payment_address']);
    unset($this->session->data['shipping_address']);

    unset($this->session->data['shipping_methods']);
    unset($this->session->data['payment_methods']);

    $address = $this->model_account_address->getAddress($this->customer->getAddressId());
    
    if($address){
      $this->session->data['payment_address'] = $address;
      $this->session->data['shipping_address'] = $address;
    }else{
      $this->json['error'][] = $this->language->get('default_address_not_valid');
      $this->sendResponse();
    }

    $this->json['data']['message'] = $this->language->get('address_set_default');
     
    $this->sendResponse();
  }

}