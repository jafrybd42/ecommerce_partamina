<?php 
/**
 * order.php
 *
 * Order Management
 *
 * @author          Ankit Chaudhary
 * @copyright       2020
 * @license         MIT
 * @version         1.0
 * @link            
 * @documentations  
 */
require_once(DIR_SYSTEM . 'engine/restapicontroller.php');

class ControllerAcApiCheckout extends RestApiController {

  /*
  @params
  payment_method
  shipping_method
  comment
  affiliate_id
  order_status_id */
  public function add(){

    $this->auth('post');
    $this->load->language('ac_api/checkout');

    $post = $this->getPost();
    $json = array();

    $this->validateData(['payment_method','shipping_method'],$post);

    // Customer
    if(!$this->customer->getId()){
      $this->json['error'][] = $this->language->get('error_customer');
      return $this->sendResponse();
    }

    // Payment Address
    $this->load->model('account/address');
    $address_info = $this->model_account_address->getAddress($post['payment_address_id']);
    if ($address_info) {
      $this->session->data['payment_address'] = $address_info;
    }else{
      $this->json['error'][] = $this->language->get('error_payment_address');
      return $this->sendResponse();
    }

    // Payment Method
    if (empty($this->session->data['payment_methods'])) {
      $this->json['error'][] = $this->language->get('error_no_payment');
      return $this->sendResponse();
    } elseif (!isset($this->session->data['payment_methods'][$post['payment_method']])) {
      $this->json['error'][] = $this->language->get('error_payment_method');
      return $this->sendResponse();
    }

    $this->session->data['payment_method'] = $this->session->data['payment_methods'][$post['payment_method']];

    if (!isset($this->session->data['payment_method'])) {
      $this->json['error'][] = $this->language->get('error_payment_method');
      return $this->sendResponse();
    }

		// Shipping
		if ($this->cart->hasShipping()) {

      // Shipping Address
      if (!isset($this->session->data['shipping_address'])) {
        $json['error'][] = $this->language->get('error_shipping_address');
      }

      // Shipping Method
      if (!$json && !empty($post['shipping_method'])) {
        if (empty($this->session->data['shipping_methods'])) {
          $json['error'][] = $this->language->get('error_no_shipping');
        } else {
          $shipping = explode('.', $post['shipping_method']);

          if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
            $json['error'][] = $this->language->get('error_shipping_method');
            $json['error_code'] = 'CA-20237';
          }
        }

        if (!$json) {
          $this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
        }
      }

      // Shipping Method
      if (!isset($this->session->data['shipping_method'])) {
        $json['error'][] = $this->language->get('error_shipping_method');
        $json['error_code'] = 'CA-20237-2';
      }

    } else {
      unset($this->session->data['shipping_address']);
      unset($this->session->data['shipping_method']);
      unset($this->session->data['shipping_methods']);
    }


    // Cart
    if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
      $json['error'][] = $this->language->get('error_stock');
    }

    // Validate minimum quantity requirements.
    $products = $this->cart->getProducts();
    
    foreach ($products as $product) {
      $product_total = 0;

      foreach ($products as $product_2) {
        if ($product_2['product_id'] == $product['product_id']) {
          $product_total += $product_2['quantity'];
        }
      }

      if ($product['minimum'] > $product_total) {
        $json['error'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);

        break;
      }
    }   
    
    if (!$json) {

      $order_data = array();

      // Store Details
      $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
      $order_data['store_id'] = $this->config->get('config_store_id');
      $order_data['store_name'] = $this->config->get('config_name');
      $order_data['store_url'] = $this->config->get('config_url');

      // Customer Details
      $order_data['customer_id'] = $this->customer->getId();
      $order_data['customer_group_id'] = $this->customer->getGroupId();
      $order_data['firstname'] = $this->customer->getFirstName();
      $order_data['lastname'] = $this->customer->getLastName();
      $order_data['email'] = $this->customer->getEmail();
      $order_data['telephone'] = $this->customer->getTelephone();
      $order_data['custom_field'] = array();
              
      // Payment Details
      $order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
      $order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
      $order_data['payment_company'] = $this->session->data['payment_address']['company'];
      $order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
      $order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
      $order_data['payment_city'] = $this->session->data['payment_address']['city'];
      $order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
      $order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
      $order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
      $order_data['payment_country'] = $this->session->data['payment_address']['country'];
      $order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
      $order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
      $order_data['payment_custom_field'] = (isset($this->session->data['payment_address']['custom_field']) ? $this->session->data['payment_address']['custom_field'] : array());

      if (isset($this->session->data['payment_method']['title'])) {
        $order_data['payment_method'] = $this->session->data['payment_method']['title'];
      } else {
        $order_data['payment_method'] = '';
      }
      
      if (isset($this->session->data['payment_method']['code'])) {
        $order_data['payment_code'] = $this->session->data['payment_method']['code'];
      } else {
        $order_data['payment_code'] = '';
      }
      
      // Shipping Details
      if ($this->cart->hasShipping()) {
        $order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
        $order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
        $order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
        $order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
        $order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
        $order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
        $order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
        $order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
        $order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
        $order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
        $order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
        $order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
        $order_data['shipping_custom_field'] = (isset($this->session->data['shipping_address']['custom_field']) ? $this->session->data['shipping_address']['custom_field'] : array());

        if (isset($this->session->data['shipping_method']['title'])) {
          $order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
        } else {
          $order_data['shipping_method'] = '';
        }

        if (isset($this->session->data['shipping_method']['code'])) {
          $order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
        } else {
          $order_data['shipping_code'] = '';
        }
      } else {
        $order_data['shipping_firstname'] = '';
        $order_data['shipping_lastname'] = '';
        $order_data['shipping_company'] = '';
        $order_data['shipping_address_1'] = '';
        $order_data['shipping_address_2'] = '';
        $order_data['shipping_city'] = '';
        $order_data['shipping_postcode'] = '';
        $order_data['shipping_zone'] = '';
        $order_data['shipping_zone_id'] = '';
        $order_data['shipping_country'] = '';
        $order_data['shipping_country_id'] = '';
        $order_data['shipping_address_format'] = '';
        $order_data['shipping_custom_field'] = array();
        $order_data['shipping_method'] = '';
        $order_data['shipping_code'] = '';
      }

      // Products
      $order_data['products'] = array();

      foreach ($this->cart->getProducts() as $product) {
        $option_data = array();

        foreach ($product['option'] as $option) {
          $option_data[] = array(
            'product_option_id'       => $option['product_option_id'],
            'product_option_value_id' => $option['product_option_value_id'],
            'option_id'               => $option['option_id'],
            'option_value_id'         => $option['option_value_id'],
            'name'                    => $option['name'],
            'value'                   => $option['value'],
            'type'                    => $option['type']
          );
        }

        $order_data['products'][] = array(
          'product_id' => $product['product_id'],
          'name'       => $product['name'],
          'model'      => $product['model'],
          'option'     => $option_data,
          'download'   => $product['download'],
          'quantity'   => $product['quantity'],
          'subtract'   => $product['subtract'],
          'price'      => $product['price'],
          'total'      => $product['total'],
          'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
          'reward'     => $product['reward']
        );
      }
      
      // Gift Voucher
      $order_data['vouchers'] = array();

      if (!empty($this->session->data['vouchers'])) {
        foreach ($this->session->data['vouchers'] as $voucher) {
          $order_data['vouchers'][] = array(
            'description'      => $voucher['description'],
            'code'             => token(10),
            'to_name'          => $voucher['to_name'],
            'to_email'         => $voucher['to_email'],
            'from_name'        => $voucher['from_name'],
            'from_email'       => $voucher['from_email'],
            'voucher_theme_id' => $voucher['voucher_theme_id'],
            'message'          => $voucher['message'],
            'amount'           => $voucher['amount']
          );
        }
      }

      // Order Totals
      $this->load->model('setting/extension'); 

      $totals = array();
      $taxes = $this->cart->getTaxes();
      $total = 0;    
      
      // Because __call can not keep var references so we put them into an array.
      $total_data = array(
        'totals' => &$totals,
        'taxes'  => &$taxes,
        'total'  => &$total
      );  
      
      $sort_order = array();
      
      $results = $this->model_setting_extension->getExtensions('total');

      foreach ($results as $key => $value) {
        $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
      }

      array_multisort($sort_order, SORT_ASC, $results);

      foreach ($results as $result) {
        if ($this->config->get('total_' . $result['code'] . '_status')) {
          $this->load->model('extension/total/' . $result['code']);
          
          // We have to put the totals in an array so that they pass by reference.
          $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
        }
      }      

      $sort_order = array();

      foreach ($total_data['totals'] as $key => $value) {
        $sort_order[$key] = $value['sort_order'];
      }

      array_multisort($sort_order, SORT_ASC, $total_data['totals']);

      $order_data = array_merge($order_data, $total_data);

      if (isset($post['comment'])) {
        $order_data['comment'] = $post['comment'];
      } else {
        $order_data['comment'] = '';
      }
    
      if (isset($post['affiliate_id']) && $post['affiliate_id']) {
        $subtotal = $this->cart->getSubTotal();

        // Affiliate
        $this->load->model('account/customer');

        $affiliate_info = $this->model_account_customer->getAffiliate($post['affiliate_id']);

        if ($affiliate_info) {
          $order_data['affiliate_id'] = $affiliate_info['customer_id'];
          $order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
        } else {
          $order_data['affiliate_id'] = 0;
          $order_data['commission'] = 0;
        }

        // Marketing
        $order_data['marketing_id'] = 0;
        $order_data['tracking'] = '';
      } else {
        $order_data['affiliate_id'] = 0;
        $order_data['commission'] = 0;
        $order_data['marketing_id'] = 0;
        $order_data['tracking'] = '';
      }

      $order_data['language_id'] = $this->config->get('config_language_id');
      $order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
      $order_data['currency_code'] = $this->session->data['currency'];
      $order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
      $order_data['ip'] = $this->request->server['REMOTE_ADDR'];
      
      if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
        $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
      } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
        $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
      } else {
        $order_data['forwarded_ip'] = '';
      }
      
      if (isset($this->request->server['HTTP_USER_AGENT'])) {
        $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
      } else {
        $order_data['user_agent'] = '';
      }
      
      if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
        $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
      } else {
        $order_data['accept_language'] = '';
      }
      
      $this->load->model('checkout/order');
      
      $json['order_id'] = $this->model_checkout_order->addOrder($order_data);
      
      // Set the order history
      if (isset($post['order_status_id'])) {
        $order_status_id = $post['order_status_id'];
      } else {
        $order_status_id = $this->config->get('config_order_status_id');
      }
       
      $this->model_checkout_order->addOrderHistory($json['order_id'], $order_status_id);

      //FCM
      $fcm_token = $this->customer->getFCMToken();
      $fcm_tokens = array();
      array_push($fcm_tokens,$fcm_token);
  
      if($fcm_tokens){
        $fcm_notificaton = array(
          "title" => $this->language->get('push_order_placed_title'),
          "body"  => sprintf($this->language->get('push_order_placed_body'),$json['order_id']),
        );
        $fcm_notification_custom_data = array(
          "type" => "order_info",
          "order_id" => $json['order_id']
        );
  
        $this->execute_fcm($fcm_tokens, $fcm_notificaton, $fcm_notification_custom_data);
      }
      //FCM end
        
      // clear cart since the order has already been successfully stored.
			$this->cart->clear();
    }

    if(isset($json['error'])){
      $this->json['error'][] = $json['error'];
      $this->json['error_code'] = (isset($json['error_code']))?$json['error_code']:'';
    }
    
    if(isset($json['order_id'])){
      $this->json['data']['message'] = $this->language->get('text_order_success');
      $this->json['data']['order_id'] = $json['order_id'];
      
      $this->cart->clear();

      unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
    }
      
    return $this->sendResponse();
  }

}