<?php
class ControllerExtensionModuleFacebookMessage extends Controller {
	public function index() {	
		$this->document->addScript('catalog/view/javascript/jquery/jquery.fbmessage.js'); 
				
		if (file_exists('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/facebook_message.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/facebook_message.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/facebook_message.css');
		}	
		
		if (file_exists('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/animate.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/animate.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/animate.css');
		}		

		$data['store_name'] = $this->config->get('config_name');
		$data['fb_app_id'] = $this->config->get('module_facebook_message_app_id');
		$data['fb_page_url'] = $this->config->get('module_facebook_message_page_url');
		$data['width'] = $this->config->get('module_facebook_message_width') ? $this->config->get('module_facebook_message_width') : 280;
		$data['height'] = $this->config->get('module_facebook_message_height') ? $this->config->get('module_facebook_message_height') : 300;
		$data['small_header'] = $this->config->get('module_facebook_message_small_header') ? 'true' : 'false';
		
		$localisation = $this->config->get('module_facebook_message_localisation');
		
		// Set Facebook API language
		if (isset($localisation[$this->config->get('config_language_id')]['localisation']) && !empty($localisation[$this->config->get('config_language_id')]['localisation'])) {
			$data['fb_locale'] = $localisation[$this->config->get('config_language_id')]['localisation'];
		} else {
			$data['fb_locale'] = 'en_US';
		}			

		$fb_username = rtrim(str_replace('https://www.facebook.com/', '', trim($this->config->get('module_facebook_message_page_url'))), '/');
		$data['fb_messenger_url'] = 'https://www.messenger.com/t/' . $fb_username; 
		
		$data['display_mode'] = $this->config->get('module_facebook_message_display_mode');
		$data['display_position'] = $this->config->get('module_facebook_message_display_position');
		
		$data['plugin_type'] = $this->config->get('module_facebook_message_type');
		
		// Set Widget text
		if (isset($localisation[$this->config->get('config_language_id')]['widget_text'])) {
			$data['widget_text'] = $localisation[$this->config->get('config_language_id')]['widget_text'];
		} else {
			$data['widget_text'] = '';
		}		

		//$data['animation'] = $this->config->get('module_facebook_message_display_animation');
		
		if ($this->config->get('module_facebook_message_status')) {		
			return $this->load->view('extension/module/facebook_message', $data);
		}
	}	 
}