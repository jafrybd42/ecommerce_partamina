<?php
class ControllerExtensionModuleEmailValidation extends Controller {
	private $error = array();

	public function index() {
		if ($this->customer->isLogged()) {
			$this->customer->logout();
		}

		if (empty($this->request->get['token']) || empty($this->request->get['email'])) {
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->library('email_validation');

		$this->load->model('extension/module/email_validation');

		$this->load->language('extension/module/email_validation');

		$this->document->setTitle($this->language->get('heading_title'));

		$email_validation_info = $this->model_extension_module_email_validation->getEmailValidationByToken($this->request->get['email'], $this->request->get['token']);

		if (!$email_validation_info) {
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		if ($email_validation_info['status'] == 'valid') {
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->_validate($email_validation_info)) {
				$this->model_extension_module_email_validation->editCustomerEmail($email_validation_info['customer_id'], $this->request->post['email']);

				$email_validation_info = $this->model_extension_module_email_validation->clearEmailValidation($email_validation_info['email_validation_id']);

				unset($this->session->data['error']);

				$this->session->data['success'] = $this->language->get('text_success');

				$this->response->redirect($this->url->link('account/login', '', true));
			}
		} else {
			$email_valid = $this->email_validation->validate($email_validation_info['email'], array('customer_id' => $email_validation_info['customer_id']));

			if ($email_valid !== true) {
				$data['error_email'] = $email_valid;
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_heading'),
			'href' => $this->url->link('extension/module/email_validation', 'email=' . $this->request->get['email'] . '&token=' . $this->request->get['token'], true)
		);

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if ($this->error) {
			foreach($this->error as $i => $msg) {
				$data['error_' . $i] = $msg;
			}
		}

		if (!empty($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif ($email_validation_info) {
			$data['email'] = $email_validation_info['email'];
		} else {
			$data['email'] = '';
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status')) {
			$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
		} else {
			$data['captcha'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_intro'] = $this->language->get('text_intro');

		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_back'] = $this->language->get('button_back');

		$data['action'] = $this->url->link('extension/module/email_validation', 'email=' . $this->request->get['email'] . '&token=' . $this->request->get['token'], true);

		$data['back'] = $this->url->link('account/login', '', true);

		$this->document->addScript('catalog/view/javascript/module/email_validation.js');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('extension/module/email_validation', $data));
	}

	public function check() {
		if (!$this->config->get('module_email_validation_status')) {
			return true;
		}

		$json = array();

		$this->load->language('extension/module/email_validation/error');

		if (empty($this->request->post['email'])) {
			$json['error'] = $this->language->get('error_required');
		} elseif (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$json['error'] = $this->language->get('error_email');
		}

		if (empty($json['error'])) {
			$json['success'] = true;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function valid() {
		if (!$this->config->get('module_email_validation_status')) {
			return true;
		}

		if (empty($this->request->post['email'])) {
			return false;
		}

		$this->load->language('extension/module/email_validation/error');

		if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$result = $this->language->get('error_email');
		} else {
			$email_validation = new \Email_Validation($this->registry);

			$result = $email_validation->validate($this->request->post['email'], array('customer_id' => $this->customer->getId()));
		}

		return $result;
	}

	private function _validate($email_validation_info) {
		if (empty($this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_required');
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('contact', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		$this->load->model('account/customer');

		if (empty($this->error['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['email'] = sprintf($this->language->get('error_exists'), $this->url->link('account/login', '', true), $this->url->link('account/forgotten', '', true));
		}

		if (empty($this->error['email'])) {
			$this->load->library('email_validation');

			$email_valid = $this->email_validation->validate($this->request->post['email'], array('customer_id' => $email_validation_info['customer_id']));

			if ($email_valid !== true) {
				$this->error['email'] = $email_valid;
			}
		}

		return !$this->error;
	}
}