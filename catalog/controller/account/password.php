<?php

class ControllerAccountPassword extends Controller {


	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/password', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/password');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/customer');

			$this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);
			//
			//edit for automated mail
			//
	
		$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

		if ($customer_info) {
// 			$this->load->model('setting/store');
	
// 			$store_info = $this->model_setting_store->getStore($customer_info['store_id']);
	
// 			if ($store_info) {
// 				$store_name = html_entity_decode($store_info['name'], ENT_QUOTES, 'UTF-8');
// 				$store_url = $store_info['url'] . 'index.php?route=account/login';
// 			} else {
// 				$store_name = html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');
// 				$store_url = HTTP_CATALOG . 'index.php?route=account/login';
// 			}
			
// 			$language = new Language($language_code);
// 			$language->load($language_code);
// 			$language->load('mail/customer_approve');
						
// 			$subject = sprintf($language->get('text_subject'), $store_name);
								
// 			$data['text_welcome'] = sprintf($language->get('text_welcome'), $store_name);
				
// 			$data['login'] = $store_url . 'index.php?route=account/login';	
// 			$data['store'] = $store_name;
			
			$mail = new Mail($this->config->get('config_mail_engine'));
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
	
			$mail->setTo($this->customer->getEmail());
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender("Petro Product Company");
			$mail->setSubject("Password Changed");
			$mail->setText("Your password is changed successfully!\r\n\r\nIf you didn't change the password please login and change your password as soon as possible.\r\n\r\n\r\nThank You\r\nPetro Products Company LTD");
			$mail->send(); 
			
		
// 			$msg = "First line of text\nSecond line of text";
// 			mail("jafrybd42@gmail.com","Password Changed",$msg);
        // 	$this->load->model('customer/customer');
	    	
// Edited Done

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('account/account', '', true));
		}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/password', '', true)
		);

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		$data['action'] = $this->url->link('account/password', '', true);
        
//         if (isset($this->request->post['current_password'])) {
// 			$data['current_password'] = $this->request->post['current_password'];
// 		} else {
// 			$data['current_password'] = '';
// 		}
		
		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}

		$data['back'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		

		$this->response->setOutput($this->load->view('account/password', $data));
	}
	
	

	protected function validate() {
	    
//
//database configuration
//
// $link = mysqli_connect("localhost", "mtcpatvp_partamina", "medinatech1*", "mtcpatvp_partamina");
 
// // Check connection
// if($link === false){
//     die("ERROR: Could not connect. " . mysqli_connect_error());
// }
// // include "config.php";

// // Escape user inputs for security
// if(isset($_POST['but_upload'])){
// $password = $_POST['current_password'];
// $email = $this->customer->getEmail();
// $active = false;
// // Attempt insert query execution

// $customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" 
// . $this->db->escape(utf8_strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" 
// . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) 
// . "') AND status = '1' AND approved = '1'");

// if(mysqli_query($link, $customer_query)){
//     $active = true;
//     // echo "\r\nAdded Successfully.";
//     // header("Location: student_courses.php");
// } else{
//     echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
// }
 
// // Close connection
// mysqli_close($link);
// }

	    
	    if ((utf8_strlen(html_entity_decode($this->request->post['current_password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['current_password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['current_password'] = $this->language->get('error_password');
		}
// 		if ($active != true) {
// 			$this->error['current_password'] = $this->language->get('error_correctpass');
// 		}

	    
		if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['password'] = $this->language->get('error_password');
		}
		
		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		return !$this->error;
	}
	
	
}