<?php

/* __string_template__426c98ffc1597ea37e5314859e1da61cb21bf195deda56135e7bae95dacbce75 */
class __TwigTemplate_dc97680d32e273d6ea7b1a1282ceaf050669b173ad26ef916e2871c15df4668a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <div class=\"brand-bg\" style=\"padding-bottom: 115px;\">
    <div class=\"container\">
      <div class=\"main-title wow bounce\">
        <h3>";
        // line 4
        echo (isset($context["text_recommend"]) ? $context["text_recommend"] : null);
        echo "<span>  </span></h3>        
        <div class=\"main-border\"><img src=\"image/catalog/brder.png\" class=\"center-block img img-responsive\"></div>
    </div>
    <div class=\"row\">
    <div id=\"brand\" class=\"owl-carousel owl-theme arrow-top\">
    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) ? $context["banners"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 10
            echo "      <div class=\"text-center\">
      ";
            // line 11
            if ($this->getAttribute($context["banner"], "link", array())) {
                // line 12
                echo "      <a href=\"";
                echo $this->getAttribute($context["banner"], "link", array());
                echo "\">
      <img src=\"";
                // line 13
                echo $this->getAttribute($context["banner"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["banner"], "title", array());
                echo "\" class=\"img-responsive\" />
      </a>
      ";
            } else {
                // line 16
                echo "      <img src=\"";
                echo $this->getAttribute($context["banner"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["banner"], "title", array());
                echo "\" class=\"img-responsive\" />
      ";
            }
            // line 18
            echo "      </div>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "      </div>
      </div> 
      </div>  
      </div>  


<script type=\"text/javascript\">
    \$(document).ready(function() {
    \$(\"#brand\").owlCarousel({
    itemsCustom : [
    [0, 2],
    [400, 3],
    [650, 4],
    [768, 5],
    [992, 5],
    [1200, 8],
    ],
             autoPlay: 2000,
            navigationText: ['<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>', '<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>'],
            navigation : true,
            pagination:false
    });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "__string_template__426c98ffc1597ea37e5314859e1da61cb21bf195deda56135e7bae95dacbce75";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 20,  62 => 18,  54 => 16,  46 => 13,  41 => 12,  39 => 11,  36 => 10,  32 => 9,  24 => 4,  19 => 1,);
    }
}
/*     <div class="brand-bg" style="padding-bottom: 115px;">*/
/*     <div class="container">*/
/*       <div class="main-title wow bounce">*/
/*         <h3>{{ text_recommend }}<span>  </span></h3>        */
/*         <div class="main-border"><img src="image/catalog/brder.png" class="center-block img img-responsive"></div>*/
/*     </div>*/
/*     <div class="row">*/
/*     <div id="brand" class="owl-carousel owl-theme arrow-top">*/
/*     {% for banner in banners %}*/
/*       <div class="text-center">*/
/*       {% if banner.link %}*/
/*       <a href="{{ banner.link }}">*/
/*       <img src="{{ banner.image }}" alt="{{ banner.title }}" class="img-responsive" />*/
/*       </a>*/
/*       {% else %}*/
/*       <img src="{{ banner.image }}" alt="{{ banner.title }}" class="img-responsive" />*/
/*       {% endif %}*/
/*       </div>*/
/*       {% endfor %}*/
/*       </div>*/
/*       </div> */
/*       </div>  */
/*       </div>  */
/* */
/* */
/* <script type="text/javascript">*/
/*     $(document).ready(function() {*/
/*     $("#brand").owlCarousel({*/
/*     itemsCustom : [*/
/*     [0, 2],*/
/*     [400, 3],*/
/*     [650, 4],*/
/*     [768, 5],*/
/*     [992, 5],*/
/*     [1200, 8],*/
/*     ],*/
/*              autoPlay: 2000,*/
/*             navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],*/
/*             navigation : true,*/
/*             pagination:false*/
/*     });*/
/*     });*/
/* </script>*/
