<?php

/* __string_template__259e131095cd28cbb0bc5d4ecb73f602ec410f39a5c0afe79fd5a745691dc51a */
class __TwigTemplate_9a293366769d3ea149025cb760f953bca82c1796c03ed4b8822fa3e5e3aadd16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--tab start -->
<div class=\"container\">
<div class=\"main-title\">
  <h3>";
        // line 4
        echo (isset($context["text_trendingProducts"]) ? $context["text_trendingProducts"] : null);
        echo "</h3>
  <div class=\"main-border\"><img src=\"image/catalog/brder.png\" class=\"center-block img img-responsive\"></div>
</div>
<ul class=\"nav nav-tabs products-tab\">
      <li class=\"active\"><a href=\"#feature\" data-toggle=\"tab\">";
        // line 8
        echo (isset($context["text_featured"]) ? $context["text_featured"] : null);
        echo "</a></li>
      <li class=\"tab-fe\"><a href=\"#special\" data-toggle=\"tab\">";
        // line 9
        echo (isset($context["text_special"]) ? $context["text_special"] : null);
        echo "</a></li>
      <li><a href=\"#best\" data-toggle=\"tab\">";
        // line 10
        echo (isset($context["text_bestSellers"]) ? $context["text_bestSellers"] : null);
        echo "</a></li>
</ul>
<!-- Tab End -->
<!--tab feature-->
<div class=\"tab-content row\">
<div class=\"tab-pane active\" id=\"feature\">

<div class=\"container\">
<div id=\"feature-product\" class=\"owl-carousel owl-theme arrow-top\">
 ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 20
            echo "  <div class=\"product-layout col-lg-12 col-md-12 col-sm-12 col-xs-12\">
    <div class=\"product-thumb transition\">
      <div class=\"image\">
        <a href=\"";
            // line 23
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">
          <img src=\"";
            // line 24
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" class=\"img-responsive\" />
        </a>
      </div>
      ";
            // line 27
            if ($this->getAttribute($context["product"], "price", array())) {
                // line 28
                echo "          ";
                if ($this->getAttribute($context["product"], "special", array())) {
                    // line 29
                    echo "               <div class=\"sale\"><img src=\"image/catalog/sale-sticker.png\" alt=\"sale\" class=\"sale product\"></div>
          ";
                }
                // line 31
                echo "      ";
            }
            // line 32
            echo "      <div class=\"caption\">
        <h4><a href=\"";
            // line 33
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["product"], "name", array());
            echo "</a></h4>
        <p  class=\"hidden-xs hidden-sm hidden-md hidden-lg\">";
            // line 34
            echo $this->getAttribute($context["product"], "description", array());
            echo "</p>
        
        <div class=\"rating\">
          ";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 38
                echo "          ";
                if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                    // line 39
                    echo "          <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          ";
                } else {
                    // line 41
                    echo "          <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          ";
                }
                // line 43
                echo "          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "        </div>
         ";
            // line 45
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                // line 46
                echo "        ";
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 47
                    echo "        <p class=\"price\">
          ";
                    // line 48
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 49
                        echo "          ";
                        echo (isset($context["currency"]) ? $context["currency"] : null);
                        echo " ";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
          ";
                    } else {
                        // line 51
                        echo "          <span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span> <span class=\"price-old\">";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
          ";
                    }
                    // line 53
                    echo "          ";
                    if ($this->getAttribute($context["product"], "tax", array())) {
                        // line 54
                        echo "          <span class=\"price-tax hidden-xs hidden-sm hidden-md hidden-lg\">";
                        echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                        echo " ";
                        echo $this->getAttribute($context["product"], "tax", array());
                        echo "</span>
          ";
                    }
                    // line 56
                    echo "           ";
                }
                // line 57
                echo "        </p>
        ";
            }
            // line 59
            echo "      </div>
      <div class=\"button-group sb-btn-group\">                
                ";
            // line 61
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                echo " <button type=\"button\" class=\"ad-to-cart\" onclick=\"cart.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-shopping-basket hidden-lg hidden-md\"></i> <span class=\"hidden-xs hidden-sm\">";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</span></button> ";
            }
            // line 62
            echo "                <button type=\"button\" class=\"theme-btn theme-btn-w\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
            echo "\" onclick=\"wishlist.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\"><i class=\"fa fa-heart\"></i></button>                
                <button type=\"button\" class=\"theme-btn theme-btn-c\" data-toggle=\"tooltip\" title=\"";
            // line 63
            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
            echo "\" onclick=\"compare.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\"><i class=\"fa fa-refresh\"></i></button>
              </div>
    </div>
  </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo " 
  </div>
  </div>
    </div>





<script type=\"text/javascript\">
    \$(document).ready(function() {
    \$(\"#feature-product\").owlCarousel({
    itemsCustom : [
    [0, 1],
    [500, 2],
    [768, 2],
    [992, 3],
    [1200, 4],
    ],
            // autoPlay: 1000,
            navigationText: ['<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>', '<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>'],
            navigation : true,
            pagination:false
    });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "__string_template__259e131095cd28cbb0bc5d4ecb73f602ec410f39a5c0afe79fd5a745691dc51a";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 67,  188 => 63,  181 => 62,  173 => 61,  169 => 59,  165 => 57,  162 => 56,  154 => 54,  151 => 53,  143 => 51,  135 => 49,  133 => 48,  130 => 47,  127 => 46,  125 => 45,  122 => 44,  116 => 43,  112 => 41,  108 => 39,  105 => 38,  101 => 37,  95 => 34,  89 => 33,  86 => 32,  83 => 31,  79 => 29,  76 => 28,  74 => 27,  64 => 24,  60 => 23,  55 => 20,  51 => 19,  39 => 10,  35 => 9,  31 => 8,  24 => 4,  19 => 1,);
    }
}
/* <!--tab start -->*/
/* <div class="container">*/
/* <div class="main-title">*/
/*   <h3>{{ text_trendingProducts }}</h3>*/
/*   <div class="main-border"><img src="image/catalog/brder.png" class="center-block img img-responsive"></div>*/
/* </div>*/
/* <ul class="nav nav-tabs products-tab">*/
/*       <li class="active"><a href="#feature" data-toggle="tab">{{ text_featured }}</a></li>*/
/*       <li class="tab-fe"><a href="#special" data-toggle="tab">{{ text_special }}</a></li>*/
/*       <li><a href="#best" data-toggle="tab">{{ text_bestSellers }}</a></li>*/
/* </ul>*/
/* <!-- Tab End -->*/
/* <!--tab feature-->*/
/* <div class="tab-content row">*/
/* <div class="tab-pane active" id="feature">*/
/* */
/* <div class="container">*/
/* <div id="feature-product" class="owl-carousel owl-theme arrow-top">*/
/*  {% for product in products %}*/
/*   <div class="product-layout col-lg-12 col-md-12 col-sm-12 col-xs-12">*/
/*     <div class="product-thumb transition">*/
/*       <div class="image">*/
/*         <a href="{{ product.href }}">*/
/*           <img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/*         </a>*/
/*       </div>*/
/*       {% if product.price %}*/
/*           {% if product.special %}*/
/*                <div class="sale"><img src="image/catalog/sale-sticker.png" alt="sale" class="sale product"></div>*/
/*           {% endif %}*/
/*       {% endif %}*/
/*       <div class="caption">*/
/*         <h4><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/*         <p  class="hidden-xs hidden-sm hidden-md hidden-lg">{{ product.description }}</p>*/
/*         */
/*         <div class="rating">*/
/*           {% for i in 1..5 %}*/
/*           {% if product.rating < i %}*/
/*           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/*           {% else %}*/
/*           <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/*           {% endif %}*/
/*           {% endfor %}*/
/*         </div>*/
/*          {% if review_guest %}*/
/*         {% if product.price %}*/
/*         <p class="price">*/
/*           {% if not product.special %}*/
/*           {{ currency }} {{ product.price }}*/
/*           {% else %}*/
/*           <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span>*/
/*           {% endif %}*/
/*           {% if product.tax %}*/
/*           <span class="price-tax hidden-xs hidden-sm hidden-md hidden-lg">{{ text_tax }} {{ product.tax }}</span>*/
/*           {% endif %}*/
/*            {% endif %}*/
/*         </p>*/
/*         {% endif %}*/
/*       </div>*/
/*       <div class="button-group sb-btn-group">                */
/*                 {% if review_guest %} <button type="button" class="ad-to-cart" onclick="cart.add('{{ product.product_id }}');"><i class="fa fa-shopping-basket hidden-lg hidden-md"></i> <span class="hidden-xs hidden-sm">{{ button_cart }}</span></button> {% endif %}*/
/*                 <button type="button" class="theme-btn theme-btn-w" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>                */
/*                 <button type="button" class="theme-btn theme-btn-c" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-refresh"></i></button>*/
/*               </div>*/
/*     </div>*/
/*   </div>*/
/*   {% endfor %} */
/*   </div>*/
/*   </div>*/
/*     </div>*/
/* */
/* */
/* */
/* */
/* */
/* <script type="text/javascript">*/
/*     $(document).ready(function() {*/
/*     $("#feature-product").owlCarousel({*/
/*     itemsCustom : [*/
/*     [0, 1],*/
/*     [500, 2],*/
/*     [768, 2],*/
/*     [992, 3],*/
/*     [1200, 4],*/
/*     ],*/
/*             // autoPlay: 1000,*/
/*             navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],*/
/*             navigation : true,*/
/*             pagination:false*/
/*     });*/
/*     });*/
/* </script>*/
