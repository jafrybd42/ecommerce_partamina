<?php

/* __string_template__ef1de6b713154ff528a2d48b5747da74281921bef3a398c20846217a40c1437b */
class __TwigTemplate_d90f8761a42541cf33c41e3bf757ac810dd73c254571a568ca40d0fc0f8ed56d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div id=\"information-contact\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  <div class=\"row\">";
        // line 8
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 9
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 10
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 11
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 12
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 13
            echo "    ";
        } else {
            // line 14
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 15
            echo "    ";
        }
        // line 16
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
      <h1 style=\"margin-top: 70px;\">";
        // line 17
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <h3>";
        // line 18
        echo (isset($context["text_location"]) ? $context["text_location"] : null);
        echo "</h3>
      <div class=\"panel panel-default\">
        <div class=\"panel-body\">
          <div class=\"row\">
            ";
        // line 22
        if ((isset($context["image"]) ? $context["image"] : null)) {
            // line 23
            echo "            <div class=\"col-sm-3\"><img src=\"";
            echo (isset($context["image"]) ? $context["image"] : null);
            echo "\" alt=\"";
            echo (isset($context["store"]) ? $context["store"] : null);
            echo "\" title=\"";
            echo (isset($context["store"]) ? $context["store"] : null);
            echo "\" class=\"img-thumbnail\" /></div>
            ";
        }
        // line 25
        echo "            <div class=\"col-sm-3\"><strong>";
        echo (isset($context["text_storeAddress"]) ? $context["text_storeAddress"] : null);
        echo "</strong><br />
              <address>
              ";
        // line 27
        echo (isset($context["text_storeLocation"]) ? $context["text_storeLocation"] : null);
        echo "
              </address>
              ";
        // line 29
        if ((isset($context["geocode"]) ? $context["geocode"] : null)) {
            // line 30
            echo "              <a href=\"https://maps.google.com/maps?q=";
            echo twig_urlencode_filter((isset($context["geocode"]) ? $context["geocode"] : null));
            echo "&hl=";
            echo (isset($context["geocode_hl"]) ? $context["geocode_hl"] : null);
            echo "&t=m&z=15\" target=\"_blank\" class=\"btn btn-info\"><i class=\"fa fa-map-marker\"></i> ";
            echo (isset($context["button_map"]) ? $context["button_map"] : null);
            echo "</a>
              ";
        }
        // line 32
        echo "            </div>
            <div class=\"col-sm-3\"><strong>";
        // line 33
        echo (isset($context["text_telephone"]) ? $context["text_telephone"] : null);
        echo "</strong><br>
              ";
        // line 34
        echo (isset($context["telephone"]) ? $context["telephone"] : null);
        echo "<br />
              <br />
              ";
        // line 36
        if ((isset($context["fax"]) ? $context["fax"] : null)) {
            // line 37
            echo "              <strong>";
            echo (isset($context["text_fax"]) ? $context["text_fax"] : null);
            echo "</strong><br>
              ";
            // line 38
            echo (isset($context["fax"]) ? $context["fax"] : null);
            echo "
              ";
        }
        // line 40
        echo "            </div>
            <div class=\"col-sm-3\">
              ";
        // line 42
        if ((isset($context["open"]) ? $context["open"] : null)) {
            // line 43
            echo "              <strong>";
            echo (isset($context["text_open"]) ? $context["text_open"] : null);
            echo "</strong><br />
              ";
            // line 44
            echo (isset($context["open"]) ? $context["open"] : null);
            echo "<br />
              <br />
              ";
        }
        // line 47
        echo "              ";
        if ((isset($context["comment"]) ? $context["comment"] : null)) {
            // line 48
            echo "              <strong>";
            echo (isset($context["text_comment"]) ? $context["text_comment"] : null);
            echo "</strong><br />
              ";
            // line 49
            echo (isset($context["comment"]) ? $context["comment"] : null);
            echo "
              ";
        }
        // line 51
        echo "            </div>
          </div>
        </div>
      </div>
      ";
        // line 55
        if ((isset($context["locations"]) ? $context["locations"] : null)) {
            // line 56
            echo "      <h3>";
            echo (isset($context["text_store"]) ? $context["text_store"] : null);
            echo "</h3>
      <div class=\"panel-group\" id=\"accordion\">
        ";
            // line 58
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["locations"]) ? $context["locations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 59
                echo "        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h4 class=\"panel-title\"><a href=\"#collapse-location";
                // line 61
                echo $this->getAttribute($context["location"], "location_id", array());
                echo "\" class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\">";
                echo $this->getAttribute($context["location"], "name", array());
                echo " <i class=\"fa fa-caret-down\"></i></a></h4>
          </div>
          <div class=\"panel-collapse collapse\" id=\"collapse-location";
                // line 63
                echo $this->getAttribute($context["location"], "location_id", array());
                echo "\">
            <div class=\"panel-body\">
              <div class=\"row\">
                ";
                // line 66
                if ($this->getAttribute($context["location"], "image", array())) {
                    // line 67
                    echo "                <div class=\"col-sm-3\"><img src=\"";
                    echo $this->getAttribute($context["location"], "image", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["location"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["location"], "name", array());
                    echo "\" class=\"img-thumbnail\" /></div>
                ";
                }
                // line 69
                echo "                <div class=\"col-sm-3\"><strong>";
                echo $this->getAttribute($context["location"], "name", array());
                echo "</strong><br />
                  <address>
                  ";
                // line 71
                echo $this->getAttribute($context["location"], "address", array());
                echo "
                  </address>
                  ";
                // line 73
                if ($this->getAttribute($context["location"], "geocode", array())) {
                    // line 74
                    echo "                  <a href=\"https://maps.google.com/maps?q=";
                    echo twig_urlencode_filter($this->getAttribute($context["location"], "geocode", array()));
                    echo "&hl=";
                    echo (isset($context["geocode_hl"]) ? $context["geocode_hl"] : null);
                    echo "&t=m&z=15\" target=\"_blank\" class=\"btn btn-info\"><i class=\"fa fa-map-marker\"></i> ";
                    echo (isset($context["button_map"]) ? $context["button_map"] : null);
                    echo "</a>
                  ";
                }
                // line 76
                echo "                </div>
                <div class=\"col-sm-3\"> <strong>";
                // line 77
                echo (isset($context["text_telephone"]) ? $context["text_telephone"] : null);
                echo "</strong><br>
                  ";
                // line 78
                echo $this->getAttribute($context["location"], "telephone", array());
                echo "<br />
                  <br />
                  ";
                // line 80
                if ($this->getAttribute($context["location"], "fax", array())) {
                    // line 81
                    echo "                  <strong>";
                    echo (isset($context["text_fax"]) ? $context["text_fax"] : null);
                    echo "</strong><br>
                  ";
                    // line 82
                    echo $this->getAttribute($context["location"], "fax", array());
                    echo "
                  ";
                }
                // line 84
                echo "                </div>
                <div class=\"col-sm-3\">
                  ";
                // line 86
                if ($this->getAttribute($context["location"], "open", array())) {
                    // line 87
                    echo "                  <strong>";
                    echo (isset($context["text_open"]) ? $context["text_open"] : null);
                    echo "</strong><br />
                  ";
                    // line 88
                    echo $this->getAttribute($context["location"], "open", array());
                    echo "<br />
                  <br />
                  ";
                }
                // line 91
                echo "                  ";
                if ($this->getAttribute($context["location"], "comment", array())) {
                    // line 92
                    echo "                  <strong>";
                    echo (isset($context["text_comment"]) ? $context["text_comment"] : null);
                    echo "</strong><br />
                  ";
                    // line 93
                    echo $this->getAttribute($context["location"], "comment", array());
                    echo "
                  ";
                }
                // line 95
                echo "                </div>
              </div>
            </div>
          </div>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 101
            echo "      </div>
      ";
        }
        // line 103
        echo "      <form action=\"catalog/view/theme/default/template/information/customer_service.php\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">
        <fieldset>
          <legend>";
        // line 105
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</legend>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-name\" >";
        // line 107
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"name\" value=\"";
        // line 109
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" required/>
              ";
        // line 110
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            // line 111
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_name"]) ? $context["error_name"] : null);
            echo "</div>
              ";
        }
        // line 113
        echo "            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 116
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"email\" value=\"";
        // line 118
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "\" id=\"input-email\" class=\"form-control\" required/>
              ";
        // line 119
        if ((isset($context["error_email"]) ? $context["error_email"] : null)) {
            // line 120
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_email"]) ? $context["error_email"] : null);
            echo "</div>
              ";
        }
        // line 122
        echo "            </div>
          </div>
          
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 126
        echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
             <select class=\"js-example-basic-multiple\" style=\"width:100%;\" name=\"subject\">
               <option value=\"US\" disabled=\"\" style=\"color: black;font-weight: bold;\">";
        // line 129
        echo (isset($context["text_orderStatus"]) ? $context["text_orderStatus"] : null);
        echo "</option>
                <option value=\"How can I track my order?\">";
        // line 130
        echo (isset($context["text_trackOrder"]) ? $context["text_trackOrder"] : null);
        echo "</option>
                <option value=\"How long does it take to recive an order?\">";
        // line 131
        echo (isset($context["text_orderRecive"]) ? $context["text_orderRecive"] : null);
        echo "</option>
               <option value=\"My order is delayed. What should I do?\">";
        // line 132
        echo (isset($context["text_OrderNotFound"]) ? $context["text_OrderNotFound"] : null);
        echo "</option>
                <option value=\"Amar order ashte deri hocche keno?\">";
        // line 133
        echo (isset($context["text_orderLate"]) ? $context["text_orderLate"] : null);
        echo "</option>
               <option value=\"Amar order delivery hote koto din lagbe?\">";
        // line 134
        echo (isset($context["text_orderDelivery"]) ? $context["text_orderDelivery"] : null);
        echo "</option>
                 <option value=\"US\" disabled=\"\" style=\"color: black;font-weight: bold;\">";
        // line 135
        echo (isset($context["text_orderReturn"]) ? $context["text_orderReturn"] : null);
        echo "</option>
                  <option value=\"How can I return my product?\">";
        // line 136
        echo (isset($context["text_orderReturning"]) ? $context["text_orderReturning"] : null);
        echo "</option>
                  <option value=\"I have not been refunded yet\">";
        // line 137
        echo (isset($context["text_orderNotReturned"]) ? $context["text_orderNotReturned"] : null);
        echo "</option>
                  <option value=\"What is refund policy?\">";
        // line 138
        echo (isset($context["text_orderRefund"]) ? $context["text_orderRefund"] : null);
        echo "</option>
                  <option value=\"When will I get my refund?\">";
        // line 139
        echo (isset($context["text_orderGetRefund"]) ? $context["text_orderGetRefund"] : null);
        echo "</option>
                  <option value=\"What is the refund process?\">";
        // line 140
        echo (isset($context["text_orderRefundProcess"]) ? $context["text_orderRefundProcess"] : null);
        echo "</option>
                 <option value=\"US\" disabled=\"\" style=\"color: black;font-weight: bold;\">";
        // line 141
        echo (isset($context["text_orderPlacement"]) ? $context["text_orderPlacement"] : null);
        echo "</option>
                  <option value=\"How to place an Order?\">";
        // line 142
        echo (isset($context["text_orderHowToPlace"]) ? $context["text_orderHowToPlace"] : null);
        echo "</option>
                  <option value=\"How do I pay ?\">";
        // line 143
        echo (isset($context["text_orderHowToPay"]) ? $context["text_orderHowToPay"] : null);
        echo "</option>
                  <option value=\"How to cancel my order?\">";
        // line 144
        echo (isset($context["text_orderCancel"]) ? $context["text_orderCancel"] : null);
        echo "</option>
                  <option value=\"Order place korar upai ki?\">";
        // line 145
        echo (isset($context["text_orderplace"]) ? $context["text_orderplace"] : null);
        echo "</option>
                  <option value=\"Order cancel korar upai ki?\">";
        // line 146
        echo (isset($context["text_ordercancel"]) ? $context["text_ordercancel"] : null);
        echo "</option>
              </select>
             
            </div>
          </div>
        
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-enquiry\">";
        // line 153
        echo (isset($context["entry_enquiry"]) ? $context["entry_enquiry"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <textarea name=\"enquiry\" rows=\"10\" id=\"input-enquiry\" class=\"form-control\" required>";
        // line 155
        echo (isset($context["enquiry"]) ? $context["enquiry"] : null);
        echo "</textarea>
              ";
        // line 156
        if ((isset($context["error_enquiry"]) ? $context["error_enquiry"] : null)) {
            // line 157
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_enquiry"]) ? $context["error_enquiry"] : null);
            echo "</div>
              ";
        }
        // line 159
        echo "            </div>
          </div>
          ";
        // line 161
        echo (isset($context["captcha"]) ? $context["captcha"] : null);
        echo "
        </fieldset>
        <div class=\"buttons\">
          <div class=\"pull-right\">
            <input class=\"btn btn-primary\" type=\"submit\" name=\"submit\" value=\"";
        // line 165
        echo (isset($context["button_submit"]) ? $context["button_submit"] : null);
        echo "\" />
          </div>
        </div>
      </form>
      ";
        // line 169
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 170
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
";
        // line 172
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "__string_template__ef1de6b713154ff528a2d48b5747da74281921bef3a398c20846217a40c1437b";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  495 => 172,  490 => 170,  486 => 169,  479 => 165,  472 => 161,  468 => 159,  462 => 157,  460 => 156,  456 => 155,  451 => 153,  441 => 146,  437 => 145,  433 => 144,  429 => 143,  425 => 142,  421 => 141,  417 => 140,  413 => 139,  409 => 138,  405 => 137,  401 => 136,  397 => 135,  393 => 134,  389 => 133,  385 => 132,  381 => 131,  377 => 130,  373 => 129,  367 => 126,  361 => 122,  355 => 120,  353 => 119,  349 => 118,  344 => 116,  339 => 113,  333 => 111,  331 => 110,  327 => 109,  322 => 107,  317 => 105,  313 => 103,  309 => 101,  298 => 95,  293 => 93,  288 => 92,  285 => 91,  279 => 88,  274 => 87,  272 => 86,  268 => 84,  263 => 82,  258 => 81,  256 => 80,  251 => 78,  247 => 77,  244 => 76,  234 => 74,  232 => 73,  227 => 71,  221 => 69,  211 => 67,  209 => 66,  203 => 63,  196 => 61,  192 => 59,  188 => 58,  182 => 56,  180 => 55,  174 => 51,  169 => 49,  164 => 48,  161 => 47,  155 => 44,  150 => 43,  148 => 42,  144 => 40,  139 => 38,  134 => 37,  132 => 36,  127 => 34,  123 => 33,  120 => 32,  110 => 30,  108 => 29,  103 => 27,  97 => 25,  87 => 23,  85 => 22,  78 => 18,  74 => 17,  67 => 16,  64 => 15,  61 => 14,  58 => 13,  55 => 12,  52 => 11,  49 => 10,  47 => 9,  43 => 8,  40 => 7,  29 => 5,  25 => 4,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div id="information-contact" class="container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*       <h1 style="margin-top: 70px;">{{ heading_title }}</h1>*/
/*       <h3>{{ text_location }}</h3>*/
/*       <div class="panel panel-default">*/
/*         <div class="panel-body">*/
/*           <div class="row">*/
/*             {% if image %}*/
/*             <div class="col-sm-3"><img src="{{ image }}" alt="{{ store }}" title="{{ store }}" class="img-thumbnail" /></div>*/
/*             {% endif %}*/
/*             <div class="col-sm-3"><strong>{{ text_storeAddress }}</strong><br />*/
/*               <address>*/
/*               {{ text_storeLocation }}*/
/*               </address>*/
/*               {% if geocode %}*/
/*               <a href="https://maps.google.com/maps?q={{ geocode|url_encode }}&hl={{ geocode_hl }}&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> {{ button_map }}</a>*/
/*               {% endif %}*/
/*             </div>*/
/*             <div class="col-sm-3"><strong>{{ text_telephone }}</strong><br>*/
/*               {{ telephone }}<br />*/
/*               <br />*/
/*               {% if fax %}*/
/*               <strong>{{ text_fax }}</strong><br>*/
/*               {{ fax }}*/
/*               {% endif %}*/
/*             </div>*/
/*             <div class="col-sm-3">*/
/*               {% if open %}*/
/*               <strong>{{ text_open }}</strong><br />*/
/*               {{ open }}<br />*/
/*               <br />*/
/*               {% endif %}*/
/*               {% if comment %}*/
/*               <strong>{{ text_comment }}</strong><br />*/
/*               {{ comment }}*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       {% if locations %}*/
/*       <h3>{{ text_store }}</h3>*/
/*       <div class="panel-group" id="accordion">*/
/*         {% for location in locations %}*/
/*         <div class="panel panel-default">*/
/*           <div class="panel-heading">*/
/*             <h4 class="panel-title"><a href="#collapse-location{{ location.location_id }}" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">{{ location.name }} <i class="fa fa-caret-down"></i></a></h4>*/
/*           </div>*/
/*           <div class="panel-collapse collapse" id="collapse-location{{ location.location_id }}">*/
/*             <div class="panel-body">*/
/*               <div class="row">*/
/*                 {% if location.image %}*/
/*                 <div class="col-sm-3"><img src="{{ location.image }}" alt="{{ location.name }}" title="{{ location.name }}" class="img-thumbnail" /></div>*/
/*                 {% endif %}*/
/*                 <div class="col-sm-3"><strong>{{ location.name }}</strong><br />*/
/*                   <address>*/
/*                   {{ location.address }}*/
/*                   </address>*/
/*                   {% if location.geocode %}*/
/*                   <a href="https://maps.google.com/maps?q={{ location.geocode|url_encode }}&hl={{ geocode_hl }}&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> {{ button_map }}</a>*/
/*                   {% endif %}*/
/*                 </div>*/
/*                 <div class="col-sm-3"> <strong>{{ text_telephone }}</strong><br>*/
/*                   {{ location.telephone }}<br />*/
/*                   <br />*/
/*                   {% if location.fax %}*/
/*                   <strong>{{ text_fax }}</strong><br>*/
/*                   {{ location.fax }}*/
/*                   {% endif %}*/
/*                 </div>*/
/*                 <div class="col-sm-3">*/
/*                   {% if location.open %}*/
/*                   <strong>{{ text_open }}</strong><br />*/
/*                   {{ location.open }}<br />*/
/*                   <br />*/
/*                   {% endif %}*/
/*                   {% if location.comment %}*/
/*                   <strong>{{ text_comment }}</strong><br />*/
/*                   {{ location.comment }}*/
/*                   {% endif %}*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*         {% endfor %}*/
/*       </div>*/
/*       {% endif %}*/
/*       <form action="catalog/view/theme/default/template/information/customer_service.php" method="post" enctype="multipart/form-data" class="form-horizontal">*/
/*         <fieldset>*/
/*           <legend>{{ text_contact }}</legend>*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-name" >{{ entry_name }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="name" value="{{ name }}" id="input-name" class="form-control" required/>*/
/*               {% if error_name %}*/
/*               <div class="text-danger">{{ error_name }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-email">{{ entry_email }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="email" value="{{ email }}" id="input-email" class="form-control" required/>*/
/*               {% if error_email %}*/
/*               <div class="text-danger">{{ error_email }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           */
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-email">{{ text_subject }}</label>*/
/*             <div class="col-sm-10">*/
/*              <select class="js-example-basic-multiple" style="width:100%;" name="subject">*/
/*                <option value="US" disabled="" style="color: black;font-weight: bold;">{{ text_orderStatus }}</option>*/
/*                 <option value="How can I track my order?">{{ text_trackOrder }}</option>*/
/*                 <option value="How long does it take to recive an order?">{{ text_orderRecive }}</option>*/
/*                <option value="My order is delayed. What should I do?">{{ text_OrderNotFound }}</option>*/
/*                 <option value="Amar order ashte deri hocche keno?">{{ text_orderLate }}</option>*/
/*                <option value="Amar order delivery hote koto din lagbe?">{{ text_orderDelivery }}</option>*/
/*                  <option value="US" disabled="" style="color: black;font-weight: bold;">{{ text_orderReturn }}</option>*/
/*                   <option value="How can I return my product?">{{ text_orderReturning }}</option>*/
/*                   <option value="I have not been refunded yet">{{ text_orderNotReturned }}</option>*/
/*                   <option value="What is refund policy?">{{ text_orderRefund }}</option>*/
/*                   <option value="When will I get my refund?">{{ text_orderGetRefund }}</option>*/
/*                   <option value="What is the refund process?">{{ text_orderRefundProcess }}</option>*/
/*                  <option value="US" disabled="" style="color: black;font-weight: bold;">{{ text_orderPlacement }}</option>*/
/*                   <option value="How to place an Order?">{{ text_orderHowToPlace }}</option>*/
/*                   <option value="How do I pay ?">{{ text_orderHowToPay }}</option>*/
/*                   <option value="How to cancel my order?">{{ text_orderCancel }}</option>*/
/*                   <option value="Order place korar upai ki?">{{ text_orderplace }}</option>*/
/*                   <option value="Order cancel korar upai ki?">{{ text_ordercancel }}</option>*/
/*               </select>*/
/*              */
/*             </div>*/
/*           </div>*/
/*         */
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-enquiry">{{ entry_enquiry }}</label>*/
/*             <div class="col-sm-10">*/
/*               <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control" required>{{ enquiry }}</textarea>*/
/*               {% if error_enquiry %}*/
/*               <div class="text-danger">{{ error_enquiry }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           {{ captcha }}*/
/*         </fieldset>*/
/*         <div class="buttons">*/
/*           <div class="pull-right">*/
/*             <input class="btn btn-primary" type="submit" name="submit" value="{{ button_submit }}" />*/
/*           </div>*/
/*         </div>*/
/*       </form>*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* {{ footer }}*/
/* */
