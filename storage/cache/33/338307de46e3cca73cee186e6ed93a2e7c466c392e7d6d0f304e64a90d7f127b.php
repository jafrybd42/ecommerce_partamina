<?php

/* common/header.twig */
class __TwigTemplate_908b4de5bff5cd09b8b1420e8484edaa1cce3819fa1a066eb6b12a036e9a07bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html dir=\"";
        // line 2
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<head>
<meta charset=\"UTF-8\" />
<title>";
        // line 5
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
<base href=\"";
        // line 6
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
";
        // line 7
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 8
            echo "<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
";
        }
        // line 10
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 11
            echo "<meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
";
        }
        // line 13
        echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0\" />
<script type=\"text/javascript\" src=\"view/javascript/jquery/jquery-2.1.1.min.js\"></script>
<script type=\"text/javascript\" src=\"view/javascript/bootstrap/js/bootstrap.min.js\"></script>
<link href=\"view/stylesheet/bootstrap.css\" type=\"text/css\" rel=\"stylesheet\" />
<link href=\"view/javascript/font-awesome/css/font-awesome.min.css\" type=\"text/css\" rel=\"stylesheet\" />
<script src=\"view/javascript/jquery/datetimepicker/moment/moment.min.js\" type=\"text/javascript\"></script>
<script src=\"view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js\" type=\"text/javascript\"></script>
<script src=\"view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js\" type=\"text/javascript\"></script>
<link href=\"view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css\" type=\"text/css\" rel=\"stylesheet\" media=\"screen\" />
<link type=\"text/css\" href=\"view/stylesheet/stylesheet.css\" rel=\"stylesheet\" media=\"screen\" />
";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 24
            echo "<link type=\"text/css\" href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 27
            echo "<link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "<script src=\"view/javascript/common.js\" type=\"text/javascript\"></script>
";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 31
            echo "<script type=\"text/javascript\" src=\"";
            echo $context["script"];
            echo "\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
          <style>
            #header .nav > li li {
                min-width: 200px;
            }
            #header .nav > li > a > .label {
                text-shadow: none;
                padding: 1px 4px;
                position: absolute;
                top: 8px;
                left: 6px;
            }
          </style>
        
</head>
<body>
<div id=\"container\">
<header id=\"header\" class=\"navbar navbar-static-top\">
  <div class=\"container-fluid\">

  
    <div id=\"header-logo\" class=\"navbar-header\"><a href=\"";
        // line 54
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\" class=\"navbar-brand\"><img src=\"view/image/logo.png\" alt=\"";
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "\" title=\"";
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "\" /></a></div>
    <a href=\"#\" id=\"button-menu\" class=\"hidden-md hidden-lg\"><span class=\"fa fa-bars\"></span></a>";
        // line 55
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 56
            echo "    
    

    
    <ul class=\"nav navbar-nav navbar-right\">

          ";
            // line 63
            echo "                            <li class=\"dropdown\">
                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                                ";
            // line 65
            if (((isset($context["alerts"]) ? $context["alerts"] : null) > 0)) {
                // line 66
                echo "                                    <span class=\"label label-danger pull-left\">";
                echo (isset($context["alerts"]) ? $context["alerts"] : null);
                echo "</span>
                                ";
            }
            // line 68
            echo "                                    <i class=\"fa fa-bell fa-lg\"></i>
                                </a>
                                <ul class=\"dropdown-menu dropdown-menu-right alerts-dropdown\" style=\"min-width: 180px;\">
                                    <li class=\"dropdown-header\" >";
            // line 71
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</li>
                                    <li>
                                        <a href=\"";
            // line 73
            echo (isset($context["processing_status"]) ? $context["processing_status"] : null);
            echo "\" style=\"display: block; overflow: auto;\">
                                            <span class=\"label label-warning pull-right\">";
            // line 74
            echo (isset($context["processing_status_total"]) ? $context["processing_status_total"] : null);
            echo "</span>";
            echo (isset($context["text_processing_status"]) ? $context["text_processing_status"] : null);
            echo "</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 77
            echo (isset($context["complete_status"]) ? $context["complete_status"] : null);
            echo "\">
                                            <span class=\"label label-success pull-right\">";
            // line 78
            echo (isset($context["complete_status_total"]) ? $context["complete_status_total"] : null);
            echo "</span>";
            echo (isset($context["text_complete_status"]) ? $context["text_complete_status"] : null);
            echo "</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 81
            echo (isset($context["return"]) ? $context["return"] : null);
            echo "\">
                                            <span class=\"label label-danger pull-right\">";
            // line 82
            echo (isset($context["return_total"]) ? $context["return_total"] : null);
            echo "</span>";
            echo (isset($context["text_return"]) ? $context["text_return"] : null);
            echo "</a>
                                    </li>
                                    <li class=\"divider\"></li>
                                    <li class=\"dropdown-header\">";
            // line 85
            echo (isset($context["text_customer"]) ? $context["text_customer"] : null);
            echo "</li>
                                    <li>
                                        <a href=\"";
            // line 87
            echo (isset($context["online"]) ? $context["online"] : null);
            echo "\">
                                            <span class=\"label label-success pull-right\">";
            // line 88
            echo (isset($context["online_total"]) ? $context["online_total"] : null);
            echo "</span>";
            echo (isset($context["text_online"]) ? $context["text_online"] : null);
            echo "</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 91
            echo (isset($context["customer_approval"]) ? $context["customer_approval"] : null);
            echo "\">
                                            <span class=\"label label-danger pull-right\">";
            // line 92
            echo (isset($context["customer_total"]) ? $context["customer_total"] : null);
            echo "</span>";
            echo (isset($context["text_approval"]) ? $context["text_approval"] : null);
            echo "</a>
                                    </li>
                                    <li class=\"divider\"></li>
                                    <li class=\"dropdown-header\">";
            // line 95
            echo (isset($context["text_product"]) ? $context["text_product"] : null);
            echo "</li>
                                    <li>
                                        <a href=\"";
            // line 97
            echo (isset($context["product"]) ? $context["product"] : null);
            echo "\">
                                            <span class=\"label label-danger pull-right\">";
            // line 98
            echo (isset($context["product_total"]) ? $context["product_total"] : null);
            echo "</span>";
            echo (isset($context["text_stock"]) ? $context["text_stock"] : null);
            echo "</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 101
            echo (isset($context["review"]) ? $context["review"] : null);
            echo "\">
                                            <span class=\"label label-danger pull-right\">";
            // line 102
            echo (isset($context["review_total"]) ? $context["review_total"] : null);
            echo "</span>";
            echo (isset($context["text_review"]) ? $context["text_review"] : null);
            echo "</a>
                                    </li>
                                </ul>
                            </li>
                            ";
            // line 107
            echo "        
      <li class=\"dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><img src=\"";
            // line 108
            echo (isset($context["image"]) ? $context["image"] : null);
            echo "\" alt=\"";
            echo (isset($context["firstname"]) ? $context["firstname"] : null);
            echo " ";
            echo (isset($context["lastname"]) ? $context["lastname"] : null);
            echo "\" title=\"";
            echo (isset($context["username"]) ? $context["username"] : null);
            echo "\" id=\"user-profile\" class=\"img-circle\" />";
            echo (isset($context["firstname"]) ? $context["firstname"] : null);
            echo " ";
            echo (isset($context["lastname"]) ? $context["lastname"] : null);
            echo " <i class=\"fa fa-caret-down fa-fw\"></i></a>
        <ul class=\"dropdown-menu dropdown-menu-right\">
          <li><a href=\"";
            // line 110
            echo (isset($context["profile"]) ? $context["profile"] : null);
            echo "\"><i class=\"fa fa-user-circle-o fa-fw\"></i> ";
            echo (isset($context["text_profile"]) ? $context["text_profile"] : null);
            echo "</a></li>
          <li role=\"separator\" class=\"divider\"></li>
          <li class=\"dropdown-header\">";
            // line 112
            echo (isset($context["text_store"]) ? $context["text_store"] : null);
            echo "</li>
          ";
            // line 113
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
                // line 114
                echo "          <li><a href=\"";
                echo $this->getAttribute($context["store"], "href", array());
                echo "\" target=\"_blank\">";
                echo $this->getAttribute($context["store"], "name", array());
                echo "</a></li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 116
            echo "          <li role=\"separator\" class=\"divider\"></li>
          <li class=\"dropdown-header\">";
            // line 117
            echo (isset($context["text_help"]) ? $context["text_help"] : null);
            echo "</li>
          <li><a href=\"http://www.opencart.com\" target=\"_blank\"><i class=\"fa fa-opencart fa-fw\"></i> ";
            // line 118
            echo (isset($context["text_homepage"]) ? $context["text_homepage"] : null);
            echo "</a></li>
          <li><a href=\"http://docs.opencart.com\" target=\"_blank\"><i class=\"fa fa-file-text-o fa-fw\"></i> ";
            // line 119
            echo (isset($context["text_documentation"]) ? $context["text_documentation"] : null);
            echo "</a></li>
          <li><a href=\"http://forum.opencart.com\" target=\"_blank\"><i class=\"fa fa-comments-o fa-fw\"></i> ";
            // line 120
            echo (isset($context["text_support"]) ? $context["text_support"] : null);
            echo "</a></li>
        </ul>
      </li>
      <li><a href=\"";
            // line 123
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\"><i class=\"fa fa-sign-out\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</span></a></li>
    </ul>
    ";
        }
        // line 125
        echo " </div>
</header>
";
    }

    public function getTemplateName()
    {
        return "common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 125,  337 => 123,  331 => 120,  327 => 119,  323 => 118,  319 => 117,  316 => 116,  305 => 114,  301 => 113,  297 => 112,  290 => 110,  275 => 108,  272 => 107,  263 => 102,  259 => 101,  251 => 98,  247 => 97,  242 => 95,  234 => 92,  230 => 91,  222 => 88,  218 => 87,  213 => 85,  205 => 82,  201 => 81,  193 => 78,  189 => 77,  181 => 74,  177 => 73,  172 => 71,  167 => 68,  161 => 66,  159 => 65,  155 => 63,  147 => 56,  145 => 55,  137 => 54,  114 => 33,  105 => 31,  101 => 30,  98 => 29,  87 => 27,  83 => 26,  70 => 24,  66 => 23,  54 => 13,  48 => 11,  46 => 10,  40 => 8,  38 => 7,  34 => 6,  30 => 5,  22 => 2,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <head>*/
/* <meta charset="UTF-8" />*/
/* <title>{{ title }}</title>*/
/* <base href="{{ base }}" />*/
/* {% if description %}*/
/* <meta name="description" content="{{ description }}" />*/
/* {% endif %}*/
/* {% if keywords %}*/
/* <meta name="keywords" content="{{ keywords }}" />*/
/* {% endif %}*/
/* <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />*/
/* <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>*/
/* <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>*/
/* <link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />*/
/* <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />*/
/* <script src="view/javascript/jquery/datetimepicker/moment/moment.min.js" type="text/javascript"></script>*/
/* <script src="view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js" type="text/javascript"></script>*/
/* <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>*/
/* <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />*/
/* <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />*/
/* {% for style in styles %}*/
/* <link type="text/css" href="{{ style.href }}" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/* {% endfor %}*/
/* {% for link in links %}*/
/* <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/* {% endfor %}*/
/* <script src="view/javascript/common.js" type="text/javascript"></script>*/
/* {% for script in scripts %}*/
/* <script type="text/javascript" src="{{ script }}"></script>*/
/* {% endfor %}*/
/* */
/*           <style>*/
/*             #header .nav > li li {*/
/*                 min-width: 200px;*/
/*             }*/
/*             #header .nav > li > a > .label {*/
/*                 text-shadow: none;*/
/*                 padding: 1px 4px;*/
/*                 position: absolute;*/
/*                 top: 8px;*/
/*                 left: 6px;*/
/*             }*/
/*           </style>*/
/*         */
/* </head>*/
/* <body>*/
/* <div id="container">*/
/* <header id="header" class="navbar navbar-static-top">*/
/*   <div class="container-fluid">*/
/* */
/*   */
/*     <div id="header-logo" class="navbar-header"><a href="{{ home }}" class="navbar-brand"><img src="view/image/logo.png" alt="{{ heading_title }}" title="{{ heading_title }}" /></a></div>*/
/*     <a href="#" id="button-menu" class="hidden-md hidden-lg"><span class="fa fa-bars"></span></a>{% if logged %}*/
/*     */
/*     */
/* */
/*     */
/*     <ul class="nav navbar-nav navbar-right">*/
/* */
/*           {# notification #}*/
/*                             <li class="dropdown">*/
/*                                 <a class="dropdown-toggle" data-toggle="dropdown">*/
/*                                 {% if alerts > 0 %}*/
/*                                     <span class="label label-danger pull-left">{{ alerts }}</span>*/
/*                                 {% endif %}*/
/*                                     <i class="fa fa-bell fa-lg"></i>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu dropdown-menu-right alerts-dropdown" style="min-width: 180px;">*/
/*                                     <li class="dropdown-header" >{{ text_order }}</li>*/
/*                                     <li>*/
/*                                         <a href="{{ processing_status }}" style="display: block; overflow: auto;">*/
/*                                             <span class="label label-warning pull-right">{{ processing_status_total }}</span>{{ text_processing_status }}</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="{{ complete_status }}">*/
/*                                             <span class="label label-success pull-right">{{ complete_status_total }}</span>{{ text_complete_status }}</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="{{ return }}">*/
/*                                             <span class="label label-danger pull-right">{{ return_total }}</span>{{ text_return }}</a>*/
/*                                     </li>*/
/*                                     <li class="divider"></li>*/
/*                                     <li class="dropdown-header">{{ text_customer }}</li>*/
/*                                     <li>*/
/*                                         <a href="{{ online }}">*/
/*                                             <span class="label label-success pull-right">{{ online_total }}</span>{{ text_online }}</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="{{ customer_approval }}">*/
/*                                             <span class="label label-danger pull-right">{{ customer_total }}</span>{{ text_approval }}</a>*/
/*                                     </li>*/
/*                                     <li class="divider"></li>*/
/*                                     <li class="dropdown-header">{{ text_product }}</li>*/
/*                                     <li>*/
/*                                         <a href="{{ product }}">*/
/*                                             <span class="label label-danger pull-right">{{ product_total }}</span>{{ text_stock }}</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="{{ review }}">*/
/*                                             <span class="label label-danger pull-right">{{ review_total }}</span>{{ text_review }}</a>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                             {# notification #}*/
/*         */
/*       <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ image }}" alt="{{ firstname }} {{ lastname }}" title="{{ username }}" id="user-profile" class="img-circle" />{{ firstname }} {{ lastname }} <i class="fa fa-caret-down fa-fw"></i></a>*/
/*         <ul class="dropdown-menu dropdown-menu-right">*/
/*           <li><a href="{{ profile }}"><i class="fa fa-user-circle-o fa-fw"></i> {{ text_profile }}</a></li>*/
/*           <li role="separator" class="divider"></li>*/
/*           <li class="dropdown-header">{{ text_store }}</li>*/
/*           {% for store in stores %}*/
/*           <li><a href="{{ store.href }}" target="_blank">{{ store.name }}</a></li>*/
/*           {% endfor %}*/
/*           <li role="separator" class="divider"></li>*/
/*           <li class="dropdown-header">{{ text_help }}</li>*/
/*           <li><a href="http://www.opencart.com" target="_blank"><i class="fa fa-opencart fa-fw"></i> {{ text_homepage }}</a></li>*/
/*           <li><a href="http://docs.opencart.com" target="_blank"><i class="fa fa-file-text-o fa-fw"></i> {{ text_documentation }}</a></li>*/
/*           <li><a href="http://forum.opencart.com" target="_blank"><i class="fa fa-comments-o fa-fw"></i> {{ text_support }}</a></li>*/
/*         </ul>*/
/*       </li>*/
/*       <li><a href="{{ logout }}"><i class="fa fa-sign-out"></i> <span class="hidden-xs hidden-sm hidden-md">{{ text_logout }}</span></a></li>*/
/*     </ul>*/
/*     {% endif %} </div>*/
/* </header>*/
/* */
