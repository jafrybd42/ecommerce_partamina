<?php

class Email_Validation {
	private $curl;
	private $log;

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		//$this->session = $registry->get('session');
	}

	/**
	 * Validate Email Address
	 *
	 * @param $email
	 * @param array $data[customer_id]
	 * @return bool|string
	 */
	public function validate($email, $data = array()) {
		if (!$this->config->get('module_email_validation_status')) {
			return true;
		}

		$language_id = isset($data['language_id']) ? $data['language_id'] : $this->config->get('config_language_id');

		$language_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE `language_id` = '" . (int)$language_id . "' LIMIT 1");

		if (version_compare(VERSION, '2.2', '>') == true) {
			$language_code = $language_query->row['code'];
		} else {
			$language_code = $language_query->row['directory'];
		}

		$language = new \Language($language_code);
		$language->load($language_code);
		$language->load('extension/module/email_validation/error');

		if (empty($email)) {
			return $language->get('error_required');
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return $language->get('error_email');
		}

		if ($this->config->get('module_email_validation_banned')) {
			$banned_domains = preg_split('/[\ \n\,]+/', $this->config->get('module_email_validation_banned'));
			$email_domain = substr($email, strrpos($email, '@') + 1);

			if (in_array($email_domain, $banned_domains)) {
				return $language->get('error_banned');
			}
		}

		$email_validation_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `email` = '" . $this->db->escape($email) . "' LIMIT 1");

		if (!$email_validation_query->row || ($this->config->get('module_email_validation_expire_days') && strtotime($email_validation_query->row['date_modified']) < strtotime('-' . $this->config->get('module_email_validation_expire_days') . ' days'))) {
			if (!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$ip_address = $this->request->server['HTTP_CLIENT_IP'];
			} elseif (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$ip_address = $this->request->server['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip_address = $this->request->server['REMOTE_ADDR'];
			}

			$params = array(
				'api_key' => $this->config->get('module_email_validation_api_key'),
				'email' => $email,
				'ip_address' => $ip_address
			);

			if ($this->config->get('module_email_validation_request_timeout')) {
				$params['requestTimeOut'] = $this->config->get('module_email_validation_request_timeout');
			}

			if ($this->config->get('module_email_validation_read_timeout')) {
				$params['readTimeOut'] = $this->config->get('module_email_validation_read_timeout');
			}

			$response = $this->_api('validate', $params);

			if (!empty($response['error']) || !isset($response['status'])) {
				if (!empty($response['error'])) {
					$this->_getLog()->write($response['error']);
				}

				return $language->get('error_default');
			}

			$result = array(
				'email' => $email,
				'status' => $response['status'],
				'sub_status' => isset($response['sub_status']) ? $response['sub_status'] : '',
				'did_you_mean' => isset($response['did_you_mean']) ? $response['did_you_mean'] : '',
				'domain_age_days' => isset($response['domain_age_days']) ? $response['domain_age_days'] : '',
				'customer_id' => isset($data['customer_id']) ? $data['customer_id'] : 0
			);

			if ($email_validation_query->row) {
				$email_validation_id = $email_validation_query->row['email_validation_id'];

				$this->db->query("UPDATE " . DB_PREFIX . "email_validation SET `status` = '" . $this->db->escape($result['status']) . "', `sub_status` = '" . $this->db->escape($result['sub_status']) . "', `did_you_mean` = '" . $this->db->escape($result['did_you_mean']) . "', `domain_age_days` = '" . $this->db->escape($result['domain_age_days']) . "', `customer_id` = " . (int)$result['customer_id'] . ", `date_modified` = NOW() WHERE `email_validation_id` = " . (int)$email_validation_id);
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "email_validation SET `email` = '" . $this->db->escape($result['email']) . "', `status` = '" . $this->db->escape($result['status']) . "', `sub_status` = '" . $this->db->escape($result['sub_status']) . "', `did_you_mean` = '" . $this->db->escape($result['did_you_mean']) . "', `domain_age_days` = '" . $this->db->escape($result['domain_age_days']) . "', `customer_id` = " . (int)$result['customer_id'] . ", `date_added` = NOW(), `date_modified` = NOW()");

				$email_validation_id = $this->db->getLastId();
			}

			$email_validation_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_validation WHERE `email_validation_id` = '" . (int)$email_validation_id . "' LIMIT 1");

			if ($this->config->get('module_email_validation_debug')) {
				$this->_getLog()->write(print_r($response, 1));
			}
		}

		$check = $this->config->get('module_email_validation_check');

		if (!empty($this->request->get['route'])) {
			$validation_route = $this->config->get('module_email_validation_route');
			$validation_route_check = $this->config->get('module_email_validation_route_check');

			$route = str_replace('/', '_', $this->request->get['route']);

			if (isset($validation_route[$route]) && $validation_route[$route] == 2 && isset($validation_route_check[$route])) {
				$check = $validation_route_check[$route];
			}
		}

		if (empty($email_validation_query->row['status']) || $email_validation_query->row['status'] != 'valid') {
			if (isset($check[$email_validation_query->row['sub_status']])) {
				$language_key = 'error_' . str_replace('-', '_', $email_validation_query->row['sub_status']);

				if ($language->get($language_key) != $language_key) {
					return $language->get($language_key);
				} else {
					$language_key = 'error_' . str_replace('-', '_', $email_validation_query->row['status']);

					if ($language->get($language_key) != $language_key) {
						return $language->get($language_key);
					} else {
						return $language->get('error_default');
					}
				}
			} else if (isset($check[$email_validation_query->row['status']])) {
				if ($email_validation_query->row['sub_status'] == 'possible_typo') {
					return sprintf($language->get('error_typo'), $email_validation_query->row['did_you_mean']);
				} else {
					$language_key = 'error_' . str_replace('-', '_', $email_validation_query->row['status']);

					if ($language->get($language_key) != $language_key) {
						return $language->get($language_key);
					} else {
						return $language->get('error_default');
					}
				}
			}
		}

		if ($this->config->get('module_email_validation_domain_age') && (!empty($email_validation_query->row['domain_age_days']) && $this->config->get('module_email_validation_domain_age') > (int)$email_validation_query->row['domain_age_days'])) {
			return $language->get('error_domain_age');
		}

		return true;
	}

	private function _getLog(){
		if (!$this->log) {
			$this->log = new \Log('email_validation.log');
		}
		return $this->log;
	}

	/**
	 * @param $method
	 * @param $params
	 * @return bool|array
	 */
	private function _api($method, $params) {
		if (empty($params) || !is_array($params)) {
			return false;
		}

		$paramsURI = http_build_query($params);

		$url = 'https://api.zerobounce.net/v2/' . $method . '?' . $paramsURI;

		if (!isset($this->curl)) {
			$this->curl = curl_init();

			curl_setopt($this->curl, CURLOPT_SSLVERSION, 6);
			curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
			if (!empty($params['requestTimeOut'])) {
				curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $params['requestTimeOut'] * 1000);
			}
			if (!empty($params['readTimeOut'])) {
				curl_setopt($this->curl, CURLOPT_TIMEOUT, $params['readTimeOut'] * 1000);
			}
		}

		curl_setopt($this->curl, CURLOPT_URL, $url);

		$response = curl_exec($this->curl);

		$responseJSON = json_decode($response, true);

		if (curl_error($this->curl)) {
			trigger_error("Curl Error: " . curl_error($this->curl));
			return false;
		}

		if (isset($response['error'])) {
			trigger_error("Error: " . $response['error']);
			return false;
		}

		return $responseJSON;
	}
}